package com.datasoft.cappitalpi.registerservice.application.handler;

import com.datasoft.cappitalpi.core.config.resource.CoreMessageIdResource;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.dto.ErrorResponse;
import com.datasoft.cappitalpi.core.exception.ForbiddenException;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLIntegrityConstraintViolationException;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);

    private final MessageTranslator messageTranslator;

    public ExceptionHandlerController(MessageTranslator messageTranslator) {
        this.messageTranslator = messageTranslator;
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorResponse> handleServiceException(Exception ex, WebRequest request) {
        final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorResponse error = new ErrorResponse(status.value(), messageTranslator.getMessage(CoreMessageIdResource.Validation.SERVER_ERROR), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(ApiException.class)
    public final ResponseEntity<ErrorResponse> handleApiException(ApiException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorResponse error = new ErrorResponse(status.value(), messageTranslator.getMessage(CoreMessageIdResource.Validation.SERVER_ERROR), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(ValidationException.class)
    public final ResponseEntity<ErrorResponse> handleValidationServiceException(ValidationException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.PRECONDITION_REQUIRED;
        ErrorResponse error = new ErrorResponse(status.value(), ex.getMessage(), request.getDescription(false));
        error.setBusinessRule(true);
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(ForbiddenException.class)
    public final ResponseEntity<ErrorResponse> handleForbiddenException(ForbiddenException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.FORBIDDEN;
        ErrorResponse error = new ErrorResponse(status.value(), ex.getMessage(), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public final ResponseEntity<ErrorResponse> handleValidationServiceException(DataIntegrityViolationException ex, WebRequest request) {
        final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        String message = ex.getMessage();
        if (ex.getMostSpecificCause() instanceof SQLIntegrityConstraintViolationException) {
            message = ex.getMostSpecificCause().getMessage();
        }
        ErrorResponse error = new ErrorResponse(status.value(), messageTranslator.getMessage(CoreMessageIdResource.Validation.DATA_INTEGRITY_VIOLATION), request.getDescription(false));
        LOGGER.error(message, ex);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(JsonMappingException.class)
    public final ResponseEntity<ErrorResponse> handleJsonMappingException(JsonMappingException ex, WebRequest request) {
        final ErrorResponse errorResponse = new ErrorResponse(HttpStatus.PRECONDITION_FAILED.value(), ex.getMessage(), request.getDescription(false));
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.PRECONDITION_FAILED);
    }

}

