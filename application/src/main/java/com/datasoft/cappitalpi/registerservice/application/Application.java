package com.datasoft.cappitalpi.registerservice.application;

import com.datasoft.cappitalpi.registerservice.integration.config.RegisterConfig;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@OpenAPIDefinition(info = @Info(title = "Register API", version = "1.0.1", description = "Register Information"))
@Import({RegisterConfig.class})
@EnableEncryptableProperties
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        LOGGER.info("Starting at time {} / {}", LocalDateTime.now(), ZoneOffset.systemDefault().getId());
        SpringApplication.run(Application.class, args);
    }

}
