package com.datasoft.cappitalpi.registerservice.application;

import com.datasoft.cappitalpi.core.config.CoreConfig;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.registerservice.integration.config.RegisterConfig;
import org.springframework.context.MessageSource;

public final class AppTestUtils {

    private AppTestUtils() {
    }

    public static MessageSource messageSource() {
        return new RegisterConfig(new CoreConfig()).messageSource();
    }

    public static String formatMessage(String messageId, String... params) {
        MessageTranslator messageTranslator = messageTranslator();
        return messageTranslator.getMessage(messageId, params);
    }

    public static MessageTranslator messageTranslator() {
        return new MessageTranslator(messageSource());
    }
}
