package com.datasoft.cappitalpi.registerservice.integration.api.impl;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.dto.ErrorResponse;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.util.HeaderConstant;
import com.datasoft.cappitalpi.registerservice.integration.api.AppAccountServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.ApiResource;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.AppAccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.InscriptionTypeDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.InvalidApiResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.MessageFormat;

@Component
public class AppAccountServiceApiImpl implements AppAccountServiceApi {

    private final RestTemplate restTemplate;
    private final ApiResource apiResource;
    private final MessageTranslator messageTranslator;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public AppAccountServiceApiImpl(RestTemplate restTemplate, ApiResource apiResource, MessageTranslator messageTranslator) {
        this.restTemplate = restTemplate;
        this.apiResource = apiResource;
        this.messageTranslator = messageTranslator;
    }

    @Override
    public AppAccountDto create(AppAccountDto appAccount) throws IOException, ApiException, ValidationException {
        if (appAccount != null) {
            try {
                final String endPoint = apiResource.getAppAccountService().getCreateUrl();
                final MultiValueMap<String, String> headers = new HttpHeaders();
                headers.add(HeaderConstant.OWNER_UID, appAccount.getUid());

                final HttpEntity<AppAccountDto> entity = new HttpEntity<>(appAccount, headers);
                ResponseEntity<AppAccountDto> response = restTemplate.exchange(endPoint, HttpMethod.POST, entity, AppAccountDto.class);
                if (HttpStatus.CREATED.equals(response.getStatusCode())) {
                    return response.getBody();
                }
                throw new InvalidApiResponseException(messageTranslator.getMessage(MessageIdResource.Validation.INVALID_API_RESPONSE, apiResource.getAppAccountService().getBaseUrl()));
            } catch (HttpClientErrorException e) {
                if (HttpStatus.PRECONDITION_REQUIRED.equals(e.getStatusCode()) && StringUtils.isNotEmpty(e.getResponseBodyAsString())) {
                    ErrorResponse errorResponse = objectMapper.readValue(e.getResponseBodyAsByteArray(), ErrorResponse.class);
                    if (errorResponse != null && errorResponse.isBusinessRule()) {
                        throw new ValidationException(errorResponse.getMessage());
                    }
                }
                throw e;
            } catch (RestClientException e) {
                throw new ApiException(messageTranslator.getMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()), e);
            } catch (Exception e) {
                throw new ApiException(messageTranslator.getMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()), e);
            }
        }
        return null;
    }

    @Override
    public InscriptionTypeDto findInscriptionTypeByUid(String uid) throws ApiException {
        if (StringUtils.isNotEmpty(uid)) {
            try {
                final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindInscriptionTypeByUidUrl(), uid);
                ResponseEntity<InscriptionTypeDto> response = restTemplate.getForEntity(endPoint, InscriptionTypeDto.class);
                if (HttpStatus.OK.equals(response.getStatusCode())) {
                    return response.getBody();
                }
            } catch (RestClientException e) {
                throw new ApiException(
                        messageTranslator.getMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()), e);
            } catch (Exception e) {
                throw new ApiException(messageTranslator.getMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()), e);
            }
        }
        return null;
    }

    @Override
    public AppAccountDto findAppAccountByInscription(String inscriptionNumber, Long registerId) throws ApiException {
        if (StringUtils.isNotEmpty(inscriptionNumber) && registerId != null) {
            try {
                final String endPoint = MessageFormat.format(
                        apiResource.getAppAccountService().getFindAppAccountByInscriptionUrl(),
                        inscriptionNumber,
                        registerId
                );
                ResponseEntity<AppAccountDto> response = restTemplate.getForEntity(endPoint, AppAccountDto.class);
                if (HttpStatus.OK.equals(response.getStatusCode())) {
                    return response.getBody();
                }
            } catch (RestClientException e) {
                throw new ApiException(messageTranslator.getMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()), e);
            } catch (Exception e) {
                throw new ApiException(messageTranslator.getMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()), e);
            }
        }
        return null;
    }

    @Override
    public AppAccountDto findAccountByName(String name, Long registerId) throws ApiException {
        if (StringUtils.isNotEmpty(name) && registerId != null) {
            try {
                final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindAccountByNameUrl(), name, registerId);
                ResponseEntity<AppAccountDto> response = restTemplate.getForEntity(endPoint, AppAccountDto.class);
                if (HttpStatus.OK.equals(response.getStatusCode())) {
                    return response.getBody();
                }
            } catch (RestClientException e) {
                throw new ApiException(messageTranslator.getMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()), e);
            } catch (Exception e) {
                throw new ApiException(messageTranslator.getMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()), e
                );
            }
        }
        return null;
    }
}
