package com.datasoft.cappitalpi.registerservice.integration.service.helper.impl;

import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.domain.Status;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.mapper.RegisterMapper;
import com.datasoft.cappitalpi.registerservice.integration.service.helper.RegisterServiceHelper;
import org.springframework.stereotype.Component;

@Component
public class RegisterServiceHelperImpl implements RegisterServiceHelper {

    @Override
    public RegisterDto getPendingRegister(Register existingRegister, AccountDto account) {
        if (account == null) {
            return null;
        }
        if (existingRegister == null) {
            account.getRegister().setStatus(Status.PENDING.name());
            return account.getRegister();
        } else {
            RegisterDto registerToUpdate = RegisterMapper.INSTANCE.toRegisterDto(existingRegister);
            registerToUpdate.setEmail(account.getRegister().getEmail());
            registerToUpdate.setInscriptionNumber(account.getRegister().getInscriptionNumber());
            registerToUpdate.setInscriptionTypeId(account.getRegister().getInscriptionTypeId());
            registerToUpdate.setName(account.getRegister().getName());
            return registerToUpdate;
        }
    }
}
