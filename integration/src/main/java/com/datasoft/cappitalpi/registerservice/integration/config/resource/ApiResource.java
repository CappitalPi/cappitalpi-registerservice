package com.datasoft.cappitalpi.registerservice.integration.config.resource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "api")
public class ApiResource {

    private final UserService userService = new UserService();
    private final AppAccountService appAccountService = new AppAccountService();

    public UserService getUserService() {
        return userService;
    }

    public AppAccountService getAppAccountService() {
        return appAccountService;
    }

    public static class UserService {

        private String baseUrl;
        private String createUrl;

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getCreateUrl() {
            return createUrl;
        }

        public void setCreateUrl(String createUrl) {
            this.createUrl = createUrl;
        }

    }

    public static class AppAccountService {

        private String baseUrl;
        private String createUrl;
        private String findInscriptionTypeByUidUrl;
        private String findAppAccountByInscriptionUrl;
        private String findAccountByNameUrl;

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getCreateUrl() {
            return createUrl;
        }

        public void setCreateUrl(String createUrl) {
            this.createUrl = createUrl;
        }

        public String getFindInscriptionTypeByUidUrl() {
            return findInscriptionTypeByUidUrl;
        }

        public void setFindInscriptionTypeByUidUrl(String findInscriptionTypeByUidUrl) {
            this.findInscriptionTypeByUidUrl = findInscriptionTypeByUidUrl;
        }

        public String getFindAppAccountByInscriptionUrl() {
            return findAppAccountByInscriptionUrl;
        }

        public void setFindAppAccountByInscriptionUrl(String findAppAccountByInscriptionUrl) {
            this.findAppAccountByInscriptionUrl = findAppAccountByInscriptionUrl;
        }

        public String getFindAccountByNameUrl() {
            return findAccountByNameUrl;
        }

        public void setFindAccountByNameUrl(String findAccountByNameUrl) {
            this.findAccountByNameUrl = findAccountByNameUrl;
        }
    }
}
