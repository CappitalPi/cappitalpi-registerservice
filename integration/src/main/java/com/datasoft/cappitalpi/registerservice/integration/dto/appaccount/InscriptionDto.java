package com.datasoft.cappitalpi.registerservice.integration.dto.appaccount;

public class InscriptionDto {

    private String uid;
    private Long code;
    private InscriptionTypeDto type;
    private AppAccountDto appAccount;
    private String value;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public InscriptionTypeDto getType() {
        return type;
    }

    public void setType(InscriptionTypeDto type) {
        this.type = type;
    }

    public AppAccountDto getAppAccount() {
        return appAccount;
    }

    public void setAppAccount(AppAccountDto appAccount) {
        this.appAccount = appAccount;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}