package com.datasoft.cappitalpi.registerservice.integration.repository.query;

import com.datasoft.cappitalpi.core.repository.QueryRequest;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public final class RegisterQuery {

    private RegisterQuery() {
    }

    public static QueryRequest findByKey(String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT r FROM Register r");
        sql.append(" WHERE  r.key = :key");

        Map<String, Object> params = new HashMap<>();
        params.put("key", key);
        return new QueryRequest.Builder().withQuery(sql.toString()).withParams(params).build();
    }

    public static QueryRequest findRegisterBy(String inscriptionNumber, String email) {
        if (StringUtils.isAnyBlank(inscriptionNumber, email)) {
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT r FROM Register r");
        sql.append(" WHERE  r.email = :email OR (r.inscriptionNumber = :inscriptionNumber)");

        Map<String, Object> params = new HashMap<>();
        params.put("email", email);
        params.put("inscriptionNumber", inscriptionNumber);
        return new QueryRequest.Builder().withQuery(sql.toString()).withParams(params).build();
    }
}
