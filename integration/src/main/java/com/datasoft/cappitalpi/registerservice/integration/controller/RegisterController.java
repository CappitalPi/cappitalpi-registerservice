package com.datasoft.cappitalpi.registerservice.integration.controller;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.RegisterRequestDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;
import com.datasoft.cappitalpi.registerservice.integration.mapper.RegisterMapper;
import com.datasoft.cappitalpi.registerservice.integration.service.RegisterService;
import com.datasoft.cappitalpi.registerservice.integration.service.state.CreateAppAccountState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/v1/registers")
public class RegisterController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateAppAccountState.class);
    private final RegisterService registerService;

    public RegisterController(RegisterService registerService) {
        this.registerService = registerService;
    }

    @GetMapping("/{code}/code")
    public ResponseEntity<RegisterDto> findByCode(@PathVariable Long code) {
        RegisterDto registerDto = registerService.findByCode(code);
        if (registerDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(registerDto);
    }

    @GetMapping("/{uid}")
    public ResponseEntity<RegisterDto> findByUid(@PathVariable String uid) {
        RegisterDto registerDto = registerService.findByUid(uid);
        if (registerDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(registerDto);
    }

    @GetMapping("/{key}/key")
    public ResponseEntity<RegisterDto> findByKey(@PathVariable String key) {
        RegisterDto registerDto = registerService.findByKey(key);
        if (registerDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(registerDto);
    }

    @PostMapping
    public ResponseEntity<RegisterDto> create(@RequestBody RegisterRequestDto registerRequest, HttpServletRequest request) throws ValidationException, ApiException, StateException {
        LOGGER.info("Request from {}", request.getHeader("X-Forwarded-For"));
        AccountDto accountDto = RegisterMapper.INSTANCE.toAccountDto(registerRequest, request.getHeader("X-Forwarded-For"));
        RegisterDto createdRegister = registerService.registerAccount(accountDto);
        if (createdRegister == null) {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(createdRegister, HttpStatus.CREATED);
    }

    @PutMapping("/{code}/disable")
    public ResponseEntity<RegisterDto> disable(@PathVariable Long code) throws ValidationException {
        RegisterDto response = registerService.disable(code);
        if (response == null) {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/{code}/enable")
    public ResponseEntity<RegisterDto> enable(@PathVariable Long code) throws ValidationException {
        RegisterDto response = registerService.enable(code);
        if (response == null) {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
