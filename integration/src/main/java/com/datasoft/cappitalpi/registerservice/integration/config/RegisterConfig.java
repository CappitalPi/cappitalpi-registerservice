package com.datasoft.cappitalpi.registerservice.integration.config;


import com.datasoft.cappitalpi.core.config.CoreConfig;
import com.datasoft.cappitalpi.core.config.I18NConfig;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Configuration
@EnableJpaRepositories(basePackages = {"com.datasoft.cappitalpi.registerservice.integration.repository"})
@EntityScan(basePackages = {"com.datasoft.cappitalpi.registerservice.integration.domain"})
@ComponentScan("com.datasoft.cappitalpi.registerservice.integration")
@Import({CoreConfig.class, I18NConfig.class})
public class RegisterConfig {

    private final ReloadableResourceBundleMessageSource messageSource;

    public RegisterConfig(CoreConfig coreConfig) {
        this.messageSource = (ReloadableResourceBundleMessageSource) coreConfig.coreMessageSource();
        this.messageSource.addBasenames("classpath:messages/messages");
    }

    public MessageSource messageSource() {
        return messageSource;
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addDeserializer(
                String.class,
                new StdDeserializer<>(String.class) {
                    @Override
                    public String deserialize(JsonParser parser, DeserializationContext context) throws IOException {
                        String result = StringDeserializer.instance.deserialize(parser, context);
                        return StringUtils.isBlank(result) ? null : result;
                    }
                });
        objectMapper.registerModule(javaTimeModule);
        return objectMapper;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}