package com.datasoft.cappitalpi.registerservice.integration.validator;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.validator.AbstractValidationBean;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import org.springframework.stereotype.Component;

@Component
public class DisableRegisterValidator extends AbstractValidationBean<Register> {


    public DisableRegisterValidator(MessageTranslator messageTranslator) {
        super(messageTranslator);
    }

    @Override
    protected void validateObject(Register register) {

    }

    @Override
    protected boolean isIdentityValid(Register register) {
        if (register == null) {
            addMessage(formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
        } else if (register.getId() == null) {
            addMessage(formatMessage(MessageIdResource.Validation.MANDATORY_ID));
        }
        skipValidations();
        return super.isIdentityValid(register);
    }

    @Override
    public Class<?> type() {
        return DisableRegisterValidator.class;
    }
}