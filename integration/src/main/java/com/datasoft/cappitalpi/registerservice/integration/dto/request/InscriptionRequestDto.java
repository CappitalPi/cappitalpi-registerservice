package com.datasoft.cappitalpi.registerservice.integration.dto.request;

public class InscriptionRequestDto {

    private String typeUid;
    private String number;

    public String getTypeUid() {
        return typeUid;
    }

    public void setTypeUid(String typeUid) {
        this.typeUid = typeUid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
