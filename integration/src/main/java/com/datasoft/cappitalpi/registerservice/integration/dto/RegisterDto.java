package com.datasoft.cappitalpi.registerservice.integration.dto;

import java.time.LocalDateTime;

public class RegisterDto {

    private String uid;
    private Long code;
    private Long inscriptionTypeId;
    private String name;
    private String email;
    private String inscriptionNumber;
    private String key;
    private String ip;
    private String status;
    private boolean disabled;
    private LocalDateTime createdDate;
    private LocalDateTime disabledDate;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Long getInscriptionTypeId() {
        return inscriptionTypeId;
    }

    public void setInscriptionTypeId(Long inscriptionTypeId) {
        this.inscriptionTypeId = inscriptionTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInscriptionNumber() {
        return inscriptionNumber;
    }

    public void setInscriptionNumber(String inscriptionNumber) {
        this.inscriptionNumber = inscriptionNumber;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getDisabledDate() {
        return disabledDate;
    }

    public void setDisabledDate(LocalDateTime disabledDate) {
        this.disabledDate = disabledDate;
    }
}
