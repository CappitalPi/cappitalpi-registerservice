package com.datasoft.cappitalpi.registerservice.integration.service.state;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;

public interface RegistrationState {

    RegistrationState next();

    void execute(RegistrationContext context) throws ApiException, ValidationException, StateException;

}
