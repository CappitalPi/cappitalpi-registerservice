package com.datasoft.cappitalpi.registerservice.integration.service.helper;

import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;

public interface RegisterServiceHelper {

    RegisterDto getPendingRegister(Register existingRegister, AccountDto account);

}
