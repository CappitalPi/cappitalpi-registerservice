package com.datasoft.cappitalpi.registerservice.integration.service.strategy;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;

import java.io.IOException;

public interface RegisterStrategy<T> {

    RegisterStrategyAction action();

    T execute(AccountDto account) throws IOException, ApiException, ValidationException;

}
