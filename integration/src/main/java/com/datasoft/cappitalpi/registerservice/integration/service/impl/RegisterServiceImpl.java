package com.datasoft.cappitalpi.registerservice.integration.service.impl;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.repository.GenericRepository;
import com.datasoft.cappitalpi.core.repository.RepositoryUtils;
import com.datasoft.cappitalpi.core.service.impl.GenericPersistenceServiceImpl;
import com.datasoft.cappitalpi.core.util.UidUtils;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.domain.Status;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;
import com.datasoft.cappitalpi.registerservice.integration.mapper.RegisterMapper;
import com.datasoft.cappitalpi.registerservice.integration.repository.RegisterRepository;
import com.datasoft.cappitalpi.registerservice.integration.service.RegisterService;
import com.datasoft.cappitalpi.registerservice.integration.service.helper.RegisterServiceHelper;
import com.datasoft.cappitalpi.registerservice.integration.service.observer.EmailObserver;
import com.datasoft.cappitalpi.registerservice.integration.service.state.RegistrationContext;
import com.datasoft.cappitalpi.registerservice.integration.validator.CreateRegisterValidator;
import com.datasoft.cappitalpi.registerservice.integration.validator.DisableRegisterValidator;
import com.datasoft.cappitalpi.registerservice.integration.validator.EnableRegisterValidator;
import com.datasoft.cappitalpi.registerservice.integration.validator.UpdateRegisterValidator;
import com.datasoft.cappitalpi.registerservice.integration.validator.ValidatorContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class RegisterServiceImpl extends GenericPersistenceServiceImpl<Register> implements RegisterService {

    private final EmailObserver emailObserver;
    private final RegistrationContext registrationContext;
    private final RegisterServiceHelper registerServiceHelper;
    private final MessageTranslator messageTranslator;
    private final RegisterRepository repository;
    private final RepositoryUtils repositoryUtils;
    private final ValidatorContext validatorContext;

    public RegisterServiceImpl(EmailObserver emailObserver,
                               RegistrationContext registrationContext,
                               RegisterServiceHelper registerServiceHelper,
                               MessageTranslator messageTranslator,
                               RegisterRepository repository,
                               RepositoryUtils repositoryUtils,
                               ValidatorContext validatorContext) {
        this.emailObserver = emailObserver;
        this.registrationContext = registrationContext;
        this.registerServiceHelper = registerServiceHelper;
        this.messageTranslator = messageTranslator;
        this.repository = repository;
        this.repositoryUtils = repositoryUtils;
        this.validatorContext = validatorContext;
    }


    @Override
    protected GenericRepository<Register> getRepository() {
        return repository;
    }

    @Override
    public RegisterDto findByCode(Long code) {
        Register register = repository.findByCode(code);
        return RegisterMapper.INSTANCE.toRegisterDto(register);
    }

    @Override
    public RegisterDto findByUid(String uid) {
        Register register = findById(UidUtils.getDecId(uid));
        return RegisterMapper.INSTANCE.toRegisterDto(register);
    }

    @Override
    public RegisterDto findByKey(String key) {
        Register register = repository.findByKey(key);
        return RegisterMapper.INSTANCE.toRegisterDto(register);
    }

    @Override
    public RegisterDto registerAccount(AccountDto account) throws ValidationException, ApiException, StateException {
        if (account == null) {
            throw new AppException(messageTranslator.getMessage(MessageIdResource.Validation.MANDATORY_OBJECT, AccountDto.class.getSimpleName()));
        }
        Register existingRegister = repository.findRegisterBy(
                account.getRegister().getEmail(),
                account.getRegister().getInscriptionNumber()
        );
        RegisterDto registerDto = registerServiceHelper.getPendingRegister(existingRegister, account);
        if (StringUtils.isNotEmpty(registerDto.getUid())) {
            registerDto = this.update(registerDto);
        } else {
            registerDto = this.create(registerDto);
        }
        registrationContext.initialize(new AccountDto.Builder()
                .withAccount(account.getAppAccount())
                .withRegister(registerDto)
                .withUser(account.getUser())
                .build()
        );
        registrationContext.execute();

        Register register = RegisterMapper.INSTANCE.toRegister(registerDto, UidUtils.getDecId(registerDto.getUid()));
        register.setStatus(Status.COMPLETED);
        Register registerCompleted = super.update(register, register.getId(), emailObserver);
        return RegisterMapper.INSTANCE.toRegisterDto(registerCompleted);
    }

    @Override
    public RegisterDto create(RegisterDto registerDto) throws ValidationException {
        if (registerDto == null) {
            throw new AppException(messageTranslator.getMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
        }
        Register register = RegisterMapper.INSTANCE.toRegister(registerDto, UidUtils.getDecId(registerDto.getUid()));
        repositoryUtils.generateCode(register);
        final UUID uuid = UUID.nameUUIDFromBytes(register.getCode().toString().getBytes(StandardCharsets.UTF_8));
        register.setKey(uuid.toString());

        ((CreateRegisterValidator) validatorContext.instance(CreateRegisterValidator.class)).validate(register);
        register.setCreatedDate(LocalDateTime.now());
        register.setEmail(register.getEmail().toLowerCase());
        Register registerPersisted = super.persist(register);
        return RegisterMapper.INSTANCE.toRegisterDto(registerPersisted);
    }

    @Override
    public RegisterDto update(RegisterDto registerDto) throws ValidationException {
        if (registerDto == null) {
            throw new AppException(messageTranslator.getMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
        }
        Register register = RegisterMapper.INSTANCE.toRegister(registerDto, UidUtils.getDecId(registerDto.getUid()));
        ((UpdateRegisterValidator) validatorContext.instance(UpdateRegisterValidator.class)).validate(register);
        register.setEmail(register.getEmail().toLowerCase());
        Register registerPersisted = super.update(register, register.getId());
        return RegisterMapper.INSTANCE.toRegisterDto(registerPersisted);
    }

    @Override
    public RegisterDto disable(Long code) throws ValidationException {
        Register register = repository.findByCode(code);
        ((DisableRegisterValidator) validatorContext.instance(DisableRegisterValidator.class)).validate(register);
        register.setDisabled(true);
        register.setDisabledDate(LocalDateTime.now());
        Register registerUpdated = super.update(register, register.getId());
        return RegisterMapper.INSTANCE.toRegisterDto(registerUpdated);
    }

    @Override
    public RegisterDto enable(Long code) throws ValidationException {
        Register register = repository.findByCode(code);
        ((EnableRegisterValidator) validatorContext.instance(EnableRegisterValidator.class)).validate(register);
        register.setDisabled(false);
        register.setDisabledDate(null);
        Register registerUpdated = super.update(register, register.getId());
        return RegisterMapper.INSTANCE.toRegisterDto(registerUpdated);
    }
}