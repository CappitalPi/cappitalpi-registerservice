package com.datasoft.cappitalpi.registerservice.integration.service.state;

import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;
import org.springframework.stereotype.Component;

@Component
public class RegistrationContext {

    private final RegistrationState createAppAccountState;
    private RegistrationState state;
    private AccountDto accountDto;

    public RegistrationContext(CreateAppAccountState createAppAccountState) {
        this.createAppAccountState = createAppAccountState;
    }

    public void initialize(AccountDto accountDto) {
        this.accountDto = accountDto;
        this.state = this.createAppAccountState;
    }

    public void execute() throws ApiException, ValidationException, StateException {
        if (this.state == null) {
            throw new AppException("State context not initialized");
        }
        this.state.execute(this);
        this.state = this.state.next();
        if (this.state != null) {
            this.execute();
        }
    }

    public AccountDto getAccountDto() {
        return accountDto;
    }

    public void setAccountDto(AccountDto accountDto) {
        this.accountDto = accountDto;
    }
}
