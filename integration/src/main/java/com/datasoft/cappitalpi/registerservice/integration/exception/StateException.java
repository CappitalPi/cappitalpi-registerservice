package com.datasoft.cappitalpi.registerservice.integration.exception;

public class StateException extends Exception {

    public StateException(String message, Throwable t) {
        super(message, t);
    }
}
