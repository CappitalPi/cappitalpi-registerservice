package com.datasoft.cappitalpi.registerservice.integration.domain;

public enum Status {

    PENDING,
    COMPLETED
}
