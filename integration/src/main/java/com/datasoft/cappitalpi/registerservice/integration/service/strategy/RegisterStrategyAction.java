package com.datasoft.cappitalpi.registerservice.integration.service.strategy;

public enum RegisterStrategyAction {

    CREATE_ACCOUNT,
    CREATE_USER
}
