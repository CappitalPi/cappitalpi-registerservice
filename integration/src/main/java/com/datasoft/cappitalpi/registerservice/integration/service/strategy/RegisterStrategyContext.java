package com.datasoft.cappitalpi.registerservice.integration.service.strategy;

import com.datasoft.cappitalpi.core.exception.AppException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class RegisterStrategyContext {

    private final Map<RegisterStrategyAction, RegisterStrategy<?>> registers;

    public RegisterStrategyContext(Set<RegisterStrategy<?>> registers) {
        if (CollectionUtils.isEmpty(registers)) {
            throw new AppException("Invalid register strategy configuration.");
        }
        this.registers = new HashMap<>();
        registers.forEach(registerStrategy -> this.registers.put(registerStrategy.action(), registerStrategy));
    }

    public RegisterStrategy<?> context(RegisterStrategyAction action) {
        RegisterStrategy<?> registerStrategy = this.registers.get(action);
        if (registerStrategy == null) {
            throw new AppException("Invalid register strategy configuration.");
        }
        return registerStrategy;
    }
}
