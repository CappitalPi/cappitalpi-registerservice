package com.datasoft.cappitalpi.registerservice.integration.service.state;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserResponseDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.RegisterStrategyContext;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.UserStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.datasoft.cappitalpi.registerservice.integration.service.strategy.RegisterStrategyAction.CREATE_USER;

@Component
public class CreateUserState implements RegistrationState {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateUserState.class);

    private final RegisterStrategyContext registerContext;
    private final MessageTranslator messageTranslator;
    private UserResponseDto user;

    public CreateUserState(RegisterStrategyContext registerContext, MessageTranslator messageTranslator) {
        this.registerContext = registerContext;
        this.messageTranslator = messageTranslator;
    }

    @Override
    public RegistrationState next() {
        LOGGER.info("The account for {} has been created successfully", user.getUsername());
        return null;
    }

    @Override
    public void execute(RegistrationContext context) throws ValidationException, ApiException, StateException {
        try {
            UserStrategy userContext = (UserStrategy) registerContext.context(CREATE_USER);
            user = userContext.execute(context.getAccountDto());
        } catch (IOException e) {
            throw new StateException(messageTranslator.getMessage(MessageIdResource.Validation.INTERNAL_ERROR, e.getMessage()), e);
        }
    }

}
