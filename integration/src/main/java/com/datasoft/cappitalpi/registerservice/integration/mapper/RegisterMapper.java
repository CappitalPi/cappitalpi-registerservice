package com.datasoft.cappitalpi.registerservice.integration.mapper;

import com.datasoft.cappitalpi.core.util.UidUtils;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.domain.Status;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.AppAccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.RegisterRequestDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserDto;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class RegisterMapper {

    public static final RegisterMapper INSTANCE = Mappers.getMapper(RegisterMapper.class);

    public abstract Register toRegister(RegisterDto registerDto, Long id);

    @AfterMapping
    public void afterToRegister(@MappingTarget Register register, RegisterDto registerDto) {
        if (registerDto != null && registerDto.getStatus() != null) {
            register.setStatus(Status.valueOf(registerDto.getStatus()));
        }
    }

    @AfterMapping
    public void afterToRegisterDto(@MappingTarget RegisterDto registerDto, Register register) {
        if (register != null && register.getStatus() != null) {
            registerDto.setStatus(register.getStatus().name());
        }
    }

    public abstract RegisterDto toRegisterDto(Register Register);

    public AccountDto toAccountDto(RegisterRequestDto request, String remoteAddress) {
        if (request == null) {
            return null;
        }
        RegisterDto register = null;
        AppAccountDto appAccount = null;
        if (request.getAppAccountRequest() != null && request.getInscription() != null) {
            register = new RegisterDto();
            register.setName(request.getAppAccountRequest().getName());
            register.setEmail(request.getEmail());
            register.setInscriptionTypeId(UidUtils.getDecId(request.getInscription().getTypeUid()));
            register.setInscriptionNumber(request.getInscription().getNumber());
            register.setIp(remoteAddress);

            appAccount = new AppAccountDto();
            appAccount.setRegisterUid(register.getUid());
            appAccount.setName(request.getAppAccountRequest().getName());
            appAccount.setBusinessName(request.getAppAccountRequest().getBusinessName());
            appAccount.setEnterpriseType(request.getAppAccountRequest().getEnterpriseType());
            appAccount.setType(request.getAppAccountRequest().getType());
        }

        UserDto user = null;
        if (request.getUser() != null) {
            user = new UserDto();
            user.setName(request.getUser().getName());
            user.setUsername(request.getUser().getUsername());
            user.setPassword(request.getUser().getPassword());
            user.setConfirmPassword(request.getUser().getConfirmPassword());
        }

        return new AccountDto.Builder()
                .withRegister(register)
                .withAccount(appAccount)
                .withUser(user)
                .build();
    }
}