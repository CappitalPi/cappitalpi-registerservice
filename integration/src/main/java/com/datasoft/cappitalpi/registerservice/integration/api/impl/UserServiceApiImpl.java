package com.datasoft.cappitalpi.registerservice.integration.api.impl;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.dto.ErrorResponse;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.util.HeaderConstant;
import com.datasoft.cappitalpi.registerservice.integration.api.UserServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.ApiResource;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserResponseDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.InvalidApiResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Component
public class UserServiceApiImpl implements UserServiceApi {

    private final RestTemplate restTemplate;
    private final ApiResource apiResource;
    private final MessageTranslator messageTranslator;
    private final ObjectMapper objectMapper;

    public UserServiceApiImpl(RestTemplate restTemplate, ApiResource apiResource, MessageTranslator messageTranslator, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.apiResource = apiResource;
        this.messageTranslator = messageTranslator;
        this.objectMapper = objectMapper;
    }

    @Override
    public UserResponseDto create(UserDto userDto) throws IOException, ApiException, ValidationException {
        if (userDto != null) {
            try {
                final String endPoint = apiResource.getUserService().getCreateUrl();
                final MultiValueMap<String, String> headers = new HttpHeaders();
                headers.add(HeaderConstant.REGISTER_UID, userDto.getRegisterUid());

                final HttpEntity<UserDto> entity = new HttpEntity<>(userDto, headers);
                ResponseEntity<UserResponseDto> response = restTemplate.exchange(endPoint, HttpMethod.POST, entity, UserResponseDto.class);
                if (HttpStatus.CREATED.equals(response.getStatusCode())) {
                    return response.getBody();
                }
                throw new InvalidApiResponseException(messageTranslator.getMessage(MessageIdResource.Validation.INVALID_API_RESPONSE, apiResource.getUserService().getBaseUrl()));
            } catch (HttpClientErrorException e) {
                if (HttpStatus.PRECONDITION_REQUIRED.equals(e.getStatusCode()) && StringUtils.isNotEmpty(e.getResponseBodyAsString())) {
                    ErrorResponse errorResponse = objectMapper.readValue(e.getResponseBodyAsByteArray(), ErrorResponse.class);
                    if (errorResponse != null && errorResponse.isBusinessRule()) {
                        throw new ValidationException(errorResponse.getMessage());
                    }
                }
                throw e;
            } catch (RestClientException e) {
                throw new ApiException(messageTranslator.getMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getUserService().getBaseUrl()), e);
            } catch (Exception e) {
                throw new ApiException(messageTranslator.getMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getUserService().getBaseUrl()), e);
            }
        }
        return null;
    }

}
