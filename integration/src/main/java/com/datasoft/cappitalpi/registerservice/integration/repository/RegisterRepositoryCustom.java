package com.datasoft.cappitalpi.registerservice.integration.repository;

import com.datasoft.cappitalpi.registerservice.integration.domain.Register;

public interface RegisterRepositoryCustom {

    Register findByKey(String key);

    Register findRegisterBy(String email, String inscriptionNumber);
}