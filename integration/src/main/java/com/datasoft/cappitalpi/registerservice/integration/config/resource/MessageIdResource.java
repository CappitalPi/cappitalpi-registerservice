package com.datasoft.cappitalpi.registerservice.integration.config.resource;

public final class MessageIdResource {

    public static final class Validation {

        public static final String ALREADY_REGISTERED = "message.validation.already-registered";
        public static final String MANDATORY_ID = "message.validation.mandatory-id";
        public static final String MANDATORY_OBJECT = "message.validation.mandatory-object";
        public static final String INVALID_ID_ON_CREATION = "message.validation.invalid-id-on-creation";
        public static final String INVALID_API_RESPONSE = "message.validation.invalid-api-response";
        public static final String INVALID_EMAIL = "message.validation.invalid-email";
        public static final String ERROR_CALLING_SERVICE = "message.validation.error-calling-service";
        public static final String INTERNAL_ERROR = "message.validation.internal-error";
        public static final String SOMETHING_WENT_WRONG_CALLING_SERVICE = "message.validation.something-went-wrong-calling-service";
    }
}
