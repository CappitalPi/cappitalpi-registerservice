package com.datasoft.cappitalpi.registerservice.integration.service;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.service.GenericPersistenceService;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;

public interface RegisterService extends GenericPersistenceService<Register> {

    RegisterDto findByCode(Long code);

    RegisterDto findByUid(String uid);

    RegisterDto findByKey(String key);

    RegisterDto registerAccount(AccountDto accountDto) throws ValidationException, ApiException, StateException;

    RegisterDto create(RegisterDto registerDto) throws ValidationException;

    RegisterDto update(RegisterDto registerDto) throws ValidationException;

    RegisterDto disable(Long code) throws ValidationException;

    RegisterDto enable(Long code) throws ValidationException;
}