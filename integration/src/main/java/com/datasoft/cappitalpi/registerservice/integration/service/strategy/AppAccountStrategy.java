package com.datasoft.cappitalpi.registerservice.integration.service.strategy;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.util.UidUtils;
import com.datasoft.cappitalpi.registerservice.integration.api.AppAccountServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.AppAccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.InscriptionDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.InscriptionTypeDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;

@Service
public class AppAccountStrategy implements RegisterStrategy<AppAccountDto> {

    private final AppAccountServiceApi appAccountServiceApi;

    public AppAccountStrategy(AppAccountServiceApi appAccountServiceApi) {
        this.appAccountServiceApi = appAccountServiceApi;
    }

    @Override
    public AppAccountDto execute(AccountDto account) throws IOException, ApiException, ValidationException {
        if (account != null) {
            AppAccountDto existingAccount = appAccountServiceApi.findAppAccountByInscription(
                    account.getRegister().getInscriptionNumber(),
                    UidUtils.getDecId(account.getRegister().getUid())
            );
            if (existingAccount != null) {
                return existingAccount;
            }
            InscriptionTypeDto type = appAccountServiceApi.findInscriptionTypeByUid(UidUtils.getEncId(account.getRegister().getInscriptionTypeId()));
            InscriptionDto inscription = new InscriptionDto();
            inscription.setType(type);
            inscription.setValue(account.getRegister().getInscriptionNumber());
            account.getAppAccount().setInscriptions(Collections.singletonList(inscription));
            account.getAppAccount().setRegisterUid(account.getRegister().getUid());
            account.getAppAccount().setName(account.getRegister().getName());
            return appAccountServiceApi.create(account.getAppAccount());
        }
        return null;
    }

    @Override
    public RegisterStrategyAction action() {
        return RegisterStrategyAction.CREATE_ACCOUNT;
    }

}
