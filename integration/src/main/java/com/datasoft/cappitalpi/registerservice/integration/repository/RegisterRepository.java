package com.datasoft.cappitalpi.registerservice.integration.repository;

import com.datasoft.cappitalpi.core.repository.GenericRepository;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterRepository extends GenericRepository<Register>, RegisterRepositoryCustom {

    Register findByCode(Long code);

}