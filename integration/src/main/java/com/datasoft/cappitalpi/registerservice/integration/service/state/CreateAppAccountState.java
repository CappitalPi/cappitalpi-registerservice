package com.datasoft.cappitalpi.registerservice.integration.service.state;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.AppAccountDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.AppAccountStrategy;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.RegisterStrategyAction;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.RegisterStrategyContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CreateAppAccountState implements RegistrationState {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateAppAccountState.class);

    private final CreateUserState createUserState;
    private final RegisterStrategyContext registerContext;
    private final MessageTranslator messageTranslator;


    public CreateAppAccountState(CreateUserState createUserState,
                                 RegisterStrategyContext registerContext,
                                 MessageTranslator messageTranslator) {
        this.createUserState = createUserState;
        this.registerContext = registerContext;
        this.messageTranslator = messageTranslator;
    }

    @Override
    public RegistrationState next() {
        return createUserState;
    }

    @Override
    public void execute(RegistrationContext context) throws StateException {
        try {
            AppAccountStrategy accountStrategy = (AppAccountStrategy) registerContext.context(RegisterStrategyAction.CREATE_ACCOUNT);
            AppAccountDto appAccount = accountStrategy.execute(context.getAccountDto());
            LOGGER.info("AppAccount {} has been created successfully", appAccount.getCode());
        } catch (Exception e) {
            throw new StateException(messageTranslator.getMessage(MessageIdResource.Validation.INTERNAL_ERROR, e.getMessage()), e);
        }
    }
}
