package com.datasoft.cappitalpi.registerservice.integration.api;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserResponseDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;

import java.io.IOException;

public interface UserServiceApi {

    UserResponseDto create(UserDto userDto) throws IOException, ApiException, ValidationException;

}
