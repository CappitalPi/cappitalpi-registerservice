package com.datasoft.cappitalpi.registerservice.integration.repository.impl;

import com.datasoft.cappitalpi.core.repository.QueryRequest;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.repository.RegisterRepositoryCustom;
import com.datasoft.cappitalpi.registerservice.integration.repository.query.RegisterQuery;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class RegisterRepositoryImpl implements RegisterRepositoryCustom {

    @PersistenceContext
    private final EntityManager entityManager;

    public RegisterRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Register findByKey(String key) {
        QueryRequest request = RegisterQuery.findByKey(key);
        if (request != null) {
            TypedQuery<Register> query = entityManager.createQuery(request.getQuery(), Register.class);
            request.getParams().forEach(query::setParameter);
            List<Register> registers = query.getResultList();
            if (!CollectionUtils.isEmpty(registers)) {
                return registers.get(0);
            }
        }
        return null;
    }

    @Override
    public Register findRegisterBy(String email, String inscriptionNumber) {
        QueryRequest request = RegisterQuery.findRegisterBy(inscriptionNumber, email);
        if (request != null) {
            TypedQuery<Register> query = entityManager.createQuery(request.getQuery(), Register.class);
            request.getParams().forEach(query::setParameter);
            List<Register> registers = query.getResultList();
            if (!CollectionUtils.isEmpty(registers)) {
                return registers.get(0);
            }
        }
        return null;
    }
}