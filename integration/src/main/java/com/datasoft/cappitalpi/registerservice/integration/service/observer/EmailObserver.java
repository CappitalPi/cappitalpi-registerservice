package com.datasoft.cappitalpi.registerservice.integration.service.observer;

import com.datasoft.cappitalpi.core.observer.BaseObserver;
import org.springframework.stereotype.Component;

@Component
public class EmailObserver implements BaseObserver {

    @Override
    public void notifyObserver(Object arg) {
        //Next version.
    }
}
