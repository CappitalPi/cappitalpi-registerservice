package com.datasoft.cappitalpi.registerservice.integration.dto;

import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.AppAccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserDto;

public class AccountDto {

    private final RegisterDto register;
    private final AppAccountDto appAccount;
    private final UserDto user;

    private AccountDto(RegisterDto register, AppAccountDto appAccount, UserDto user) {
        this.register = register;
        this.appAccount = appAccount;
        this.user = user;
    }

    public RegisterDto getRegister() {
        return register;
    }

    public AppAccountDto getAppAccount() {
        return appAccount;
    }

    public UserDto getUser() {
        return user;
    }


    public static class Builder {

        private RegisterDto register;
        private AppAccountDto appAccount;
        private UserDto user;

        public Builder withRegister(RegisterDto register) {
            this.register = register;
            return this;
        }

        public Builder withAccount(AppAccountDto appAccount) {
            this.appAccount = appAccount;
            return this;
        }

        public Builder withUser(UserDto user) {
            this.user = user;
            return this;
        }

        public AccountDto build() {
            return new AccountDto(register, appAccount, user);
        }
    }
}
