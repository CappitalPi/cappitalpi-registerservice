package com.datasoft.cappitalpi.registerservice.integration.exception;

public class ApiException extends Exception {

    public ApiException(String message, Throwable t) {
        super(message, t);
    }
}
