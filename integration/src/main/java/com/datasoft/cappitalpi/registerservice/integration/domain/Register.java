package com.datasoft.cappitalpi.registerservice.integration.domain;

import com.datasoft.cappitalpi.core.domain.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "register")
public class Register extends BaseEntity {

    @Column(name = "insc_type_id")
    private Long inscriptionTypeId;

    @Column(name = "name", nullable = false, length = 80)
    private String name;

    @Column(name = "email", nullable = false, length = 80)
    private String email;

    @Column(name = "inscription_number", nullable = false, length = 30)
    private String inscriptionNumber;

    @Column(name = "key_value", nullable = false, length = 40)
    private String key;

    @Column(name = "ip", nullable = false)
    private String ip;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 10)
    private Status status;

    @Column(name = "disabled")
    private boolean disabled;

    @Column(name = "created_date", updatable = false)
    private LocalDateTime createdDate;

    @Column(name = "disabled_date", updatable = false)
    private LocalDateTime disabledDate;

    public Long getInscriptionTypeId() {
        return inscriptionTypeId;
    }

    public void setInscriptionTypeId(Long inscriptionTypeId) {
        this.inscriptionTypeId = inscriptionTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInscriptionNumber() {
        return inscriptionNumber;
    }

    public void setInscriptionNumber(String inscriptionNumber) {
        this.inscriptionNumber = inscriptionNumber;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getDisabledDate() {
        return disabledDate;
    }

    public void setDisabledDate(LocalDateTime disabledDate) {
        this.disabledDate = disabledDate;
    }
}