package com.datasoft.cappitalpi.registerservice.integration.dto.request;

public class RegisterRequestDto {

    private String email;
    private AppAccountRequestDto appAccountRequest;
    private InscriptionRequestDto inscription;
    private UserRequestDto user;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public InscriptionRequestDto getInscription() {
        return inscription;
    }

    public void setInscription(InscriptionRequestDto inscription) {
        this.inscription = inscription;
    }

    public AppAccountRequestDto getAppAccountRequest() {
        return appAccountRequest;
    }

    public void setAppAccountRequest(AppAccountRequestDto appAccountRequest) {
        this.appAccountRequest = appAccountRequest;
    }

    public UserRequestDto getUser() {
        return user;
    }

    public void setUser(UserRequestDto user) {
        this.user = user;
    }
}
