package com.datasoft.cappitalpi.registerservice.integration.exception;

public class InvalidApiResponseException extends Exception {

    public InvalidApiResponseException(String message) {
        super(message);
    }
}
