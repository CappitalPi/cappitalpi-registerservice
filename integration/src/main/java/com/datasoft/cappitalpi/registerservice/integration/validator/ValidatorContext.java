package com.datasoft.cappitalpi.registerservice.integration.validator;

import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.core.validator.ValidationBean;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ValidatorContext {

    private final Map<Class<?>, ValidationBean<?>> validators;

    public ValidatorContext(Set<ValidationBean<?>> validators) {
        if (CollectionUtils.isEmpty(validators)) {
            throw new AppException("Invalid validator configuration.");
        }
        this.validators = new HashMap<>();
        validators.forEach(validationBean -> this.validators.put(validationBean.type(), validationBean));
    }

    public ValidationBean<?> instance(Class<?> type) {
        ValidationBean<?> registerStrategy = this.validators.get(type);
        if (registerStrategy == null) {
            throw new AppException("Invalid validator context configuration.");
        }
        return registerStrategy;
    }
}
