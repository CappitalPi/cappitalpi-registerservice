package com.datasoft.cappitalpi.registerservice.integration.api;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.AppAccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.InscriptionTypeDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;

import java.io.IOException;

public interface AppAccountServiceApi {

    AppAccountDto create(AppAccountDto appAccount) throws IOException, ApiException, ValidationException;

    InscriptionTypeDto findInscriptionTypeByUid(String uid) throws ApiException;

    AppAccountDto findAccountByName(String name, Long registerId) throws ApiException;

    AppAccountDto findAppAccountByInscription(String inscriptionNumber, Long registerId) throws ApiException;
}
