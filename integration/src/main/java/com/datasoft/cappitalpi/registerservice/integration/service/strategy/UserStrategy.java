package com.datasoft.cappitalpi.registerservice.integration.service.strategy;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.api.UserServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserResponseDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserStrategy implements RegisterStrategy<UserResponseDto> {

    private final UserServiceApi userServiceApi;

    public UserStrategy(UserServiceApi userServiceApi) {
        this.userServiceApi = userServiceApi;
    }

    @Override
    public UserResponseDto execute(AccountDto accountDto) throws IOException, ValidationException, ApiException {
        if (accountDto != null && accountDto.getAppAccount() != null) {
            accountDto.getUser().setRegisterUid(accountDto.getRegister().getUid());
            accountDto.getUser().setEmail(accountDto.getRegister().getEmail());
            return userServiceApi.create(accountDto.getUser());
        }
        return null;
    }

    @Override
    public RegisterStrategyAction action() {
        return RegisterStrategyAction.CREATE_USER;
    }

}
