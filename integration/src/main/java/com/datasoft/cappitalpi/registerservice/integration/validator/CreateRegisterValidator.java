package com.datasoft.cappitalpi.registerservice.integration.validator;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.validator.AbstractValidationBean;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

@Component
public class CreateRegisterValidator extends AbstractValidationBean<Register> {


    public CreateRegisterValidator(MessageTranslator messageTranslator) {
        super(messageTranslator);
    }

    @Override
    protected void validateObject(Register register) {
        if (!EmailValidator.getInstance().isValid(register.getEmail())) {
            addMessage(formatMessage(MessageIdResource.Validation.INVALID_EMAIL));
        }
    }

    @Override
    protected boolean isIdentityValid(Register register) {
        if (register == null) {
            addMessage(formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
        } else if (register.getId() != null) {
            addMessage(formatMessage(MessageIdResource.Validation.INVALID_ID_ON_CREATION, register.getId().toString()));
        }
        return super.isIdentityValid(register);
    }

    @Override
    public Class<?> type() {
        return CreateRegisterValidator.class;
    }
}