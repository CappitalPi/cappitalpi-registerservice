package com.datasoft.cappitalpi.registerservice.integration.validator;

import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupportContext;
import com.datasoft.cappitalpi.core.config.resource.CoreMessageIdResource;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.DomainFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.google.common.truth.Truth;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
class CreateRegisterValidatorTest {

    private CreateRegisterValidator createRegisterValidator;

    @BeforeEach
    void setUp() {
        AnnotationSupportContext context = TestUtils.getValidatorContext();
        createRegisterValidator = new CreateRegisterValidator(TestUtils.messageTranslator());
        createRegisterValidator.setContext(context);
    }

    @Test
    void givenType_thenReturnCreateRegisterValidator() {
        Truth.assertThat(createRegisterValidator.type()).isEqualTo(CreateRegisterValidator.class);
    }

    @Test
    public void givenNullRegister_whenValidate_thenThrowException() {
        ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(null));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
    }

    @Test
    public void givenRegisterId_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.existingRegister();
        ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(register));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.INVALID_ID_ON_CREATION, register.getId().toString()));
    }

    @Test
    public void givenRegisterWithoutCode_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.newRegister();
        register.setId(null);
        register.setCode(null);
        ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(register));
        String message = TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "code");
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenRegisterWithoutKey_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.newRegister();
        register.setKey(null);
        ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(register));
        String message = TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "key");
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenRegisterWithoutEmail_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.newRegister();
        register.setEmail(null);
        ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(register));
        String message = TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "email");
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenRegisterWithoutInscriptionNumber_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.newRegister();
        register.setInscriptionNumber(null);
        ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(register));
        String message = TestUtils.formatMessage(CoreMessageIdResource.Validation.MANDATORY_FIELD, "inscriptionNumber");
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenInvalidEmailFormat_whenValidate_thenThrowException() {
        List<String> invalidMails = TestUtils.invalidEmailList();
        invalidMails.forEach(invalidEmail -> {
            Register register = DomainFactoryUtils.newRegister();
            register.setEmail(invalidEmail);
            ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(register));
            Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.INVALID_EMAIL));
        });
    }

    @Test
    public void givenRegisterInvalidKeyLength_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.newRegister();
        register.setId(null);
        register.setKey(RandomStringUtils.randomAlphabetic(41));
        ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(register));
        String message = TestUtils.formatMessage(CoreMessageIdResource.Validation.CANNOT_BE_GREATER_THAN, "key", "40");
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenRegisterInvalidInscriptionNumberLength_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.newRegister();
        register.setInscriptionNumber(RandomStringUtils.randomAlphabetic(31));
        ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(register));
        String message = TestUtils.formatMessage(CoreMessageIdResource.Validation.CANNOT_BE_GREATER_THAN, "inscriptionNumber", "30");
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenRegisterInvalidEmailLength_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.newRegister();
        register.setId(null);
        register.setEmail(RandomStringUtils.randomAlphabetic(81));
        ValidationException e = assertThrows(ValidationException.class, () -> createRegisterValidator.validate(register));
        String message = TestUtils.formatMessage(CoreMessageIdResource.Validation.CANNOT_BE_GREATER_THAN, "email", "80");
        Truth.assertThat(e.getMessage()).isEqualTo(message);
    }

    @Test
    public void givenRegister_whenValidate_thenRegisterIsValid() {
        assertDoesNotThrow(() -> createRegisterValidator.validate(DomainFactoryUtils.newRegister()));
    }

}