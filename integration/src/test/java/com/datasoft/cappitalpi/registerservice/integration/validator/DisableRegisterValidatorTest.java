package com.datasoft.cappitalpi.registerservice.integration.validator;

import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupportContext;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.DomainFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
class DisableRegisterValidatorTest {

    private DisableRegisterValidator disableRegisterValidator;

    @BeforeEach
    void setUp() {
        AnnotationSupportContext context = TestUtils.getValidatorContext();
        disableRegisterValidator = new DisableRegisterValidator(TestUtils.messageTranslator());
        disableRegisterValidator.setContext(context);
    }

    @Test
    void givenType_thenReturnDisableRegisterValidator() {
        Truth.assertThat(disableRegisterValidator.type()).isEqualTo(DisableRegisterValidator.class);
    }

    @Test
    public void givenNullRegister_whenValidate_thenThrowException() {
        ValidationException e = assertThrows(ValidationException.class, () -> disableRegisterValidator.validate(null));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
    }

    @Test
    public void givenNullRegisterId_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.newRegister();
        ValidationException e = assertThrows(ValidationException.class, () -> disableRegisterValidator.validate(register));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_ID));
    }

    @Test
    public void givenEmptyMandatoryField_whenValidate_thenSkipValidations() throws ValidationException {
        Register register = DomainFactoryUtils.existingRegister();
        register.setKey(null);
        disableRegisterValidator.validate(register);
    }

    @Test
    public void givenValidRegister_whenValidate_thenRegisterIsValidated() throws ValidationException {
        Register register = DomainFactoryUtils.existingRegister();
        disableRegisterValidator.validate(register);
    }
}