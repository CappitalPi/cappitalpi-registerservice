package com.datasoft.cappitalpi.registerservice.integration.service.impl;

import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupportContext;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.repository.RepositoryUtils;
import com.datasoft.cappitalpi.core.util.UidUtils;
import com.datasoft.cappitalpi.core.validator.ValidationBean;
import com.datasoft.cappitalpi.registerservice.integration.CompareObjectUtils;
import com.datasoft.cappitalpi.registerservice.integration.DomainFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.domain.Status;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;
import com.datasoft.cappitalpi.registerservice.integration.mapper.RegisterMapper;
import com.datasoft.cappitalpi.registerservice.integration.repository.RegisterRepository;
import com.datasoft.cappitalpi.registerservice.integration.service.RegisterService;
import com.datasoft.cappitalpi.registerservice.integration.service.helper.impl.RegisterServiceHelperImpl;
import com.datasoft.cappitalpi.registerservice.integration.service.observer.EmailObserver;
import com.datasoft.cappitalpi.registerservice.integration.service.state.RegistrationContext;
import com.datasoft.cappitalpi.registerservice.integration.validator.CreateRegisterValidator;
import com.datasoft.cappitalpi.registerservice.integration.validator.DisableRegisterValidator;
import com.datasoft.cappitalpi.registerservice.integration.validator.EnableRegisterValidator;
import com.datasoft.cappitalpi.registerservice.integration.validator.UpdateRegisterValidator;
import com.datasoft.cappitalpi.registerservice.integration.validator.ValidatorContext;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RegisterServiceImplImplTest {

    @Mock
    private RegisterRepository repository;
    @Mock
    private RepositoryUtils repositoryUtils;
    @Mock
    private RegistrationContext registrationContext;
    @Mock
    private EmailObserver emailObserver;
    private RegisterService registerService;

    @BeforeEach
    void setUp() {
        MessageTranslator messageTranslator = TestUtils.messageTranslator();
        AnnotationSupportContext context = TestUtils.getValidatorContext();

        CreateRegisterValidator createRegisterValidator = new CreateRegisterValidator(messageTranslator);
        createRegisterValidator.setContext(context);

        UpdateRegisterValidator updateRegisterValidator = new UpdateRegisterValidator(messageTranslator);
        updateRegisterValidator.setContext(context);

        DisableRegisterValidator disableRegisterValidator = new DisableRegisterValidator(messageTranslator);
        disableRegisterValidator.setContext(context);

        EnableRegisterValidator enableRegisterValidator = new EnableRegisterValidator(messageTranslator);
        enableRegisterValidator.setContext(context);

        Set<ValidationBean<?>> validators = new HashSet<>();
        validators.add(createRegisterValidator);
        validators.add(updateRegisterValidator);
        validators.add(disableRegisterValidator);
        validators.add(enableRegisterValidator);

        registerService = new RegisterServiceImpl(
                emailObserver,
                registrationContext,
                new RegisterServiceHelperImpl(),
                messageTranslator,
                repository,
                repositoryUtils,
                new ValidatorContext(validators)
        );
    }

    @Test
    public void givenInvalidCode_whenFindByCode_thenReturnNull() {
        RegisterDto registerDto = registerService.findByCode(null);
        verify(repository, times(1)).findByCode(any());
        Truth.assertThat(registerDto).isNull();
    }

    @Test
    public void givenValidCode_whenFindByCode_thenReturnRegister() {
        Register register = DomainFactoryUtils.newRegister();
        when(repository.findByCode(any())).thenReturn(register);
        RegisterDto dto = registerService.findByCode(100L);
        verify(repository, times(1)).findByCode(any());
        Truth.assertThat(register).isNotNull();
        CompareObjectUtils.compare(register, dto);
    }

    @Test
    public void givenInvalidUid_whenFindByUid_thenReturnNull() {
        RegisterDto registerDto = registerService.findByUid(null);
        verify(repository, times(1)).findById(any());
        Truth.assertThat(registerDto).isNull();
    }

    @Test
    public void givenValidCode_whenFindByUid_thenReturnRegister() {
        Register register = DomainFactoryUtils.newRegister();
        when(repository.findById(any())).thenReturn(Optional.of(register));
        RegisterDto response = registerService.findByUid("uid-for-test");
        verify(repository, times(1)).findById(any());
        Truth.assertThat(register).isNotNull();
        CompareObjectUtils.compare(register, response);
    }

    @Test
    public void givenInvalidName_whenFindByEmailOrName_thenReturnNull() {
        RegisterDto response = registerService.findByKey(null);
        verify(repository, times(1)).findByKey(any());
        Truth.assertThat(response).isNull();
    }

    @Test
    public void givenValidCode_whenFindByEmailOrName_thenReturnRegister() {
        Register register = DomainFactoryUtils.newRegister();
        when(repository.findByKey(any())).thenReturn(register);
        RegisterDto response = registerService.findByKey("89sdf987sd897f");
        verify(repository, times(1)).findByKey(any());
        Truth.assertThat(register).isNotNull();
        CompareObjectUtils.compare(register, response);
    }

    @Test
    public void givenNullRegister_whenCreate_thenThrowException() {
        AppException e = assertThrows(AppException.class, () -> registerService.create(null));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
        verify(repository, times(0)).save(any());
    }

    @Test
    public void givenInvalidKey_whenCreate_thenThrowException() {
        RegisterDto registerDto = DtoFactoryUtils.registerDto();
        registerDto.setKey(null);
        ValidationException e = assertThrows(ValidationException.class, () -> registerService.create(registerDto));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.INVALID_ID_ON_CREATION, UidUtils.getDecId(registerDto.getUid()).toString()));
        verify(repository, times(0)).save(any());
    }

    @Test
    public void givenValidRegister_whenCreate_thenRegisterIsCreated() throws ValidationException {
        Register newRegister = DomainFactoryUtils.newRegister();

        when(repository.save(any())).thenReturn(newRegister);

        RegisterDto registerToCreate = RegisterMapper.INSTANCE.toRegisterDto(newRegister);
        RegisterDto registerDto = registerService.create(registerToCreate);

        verify(repository, times(1)).save(any());
        Truth.assertThat(registerDto).isNotNull();
    }

    @Test
    public void givenNullRegister_whenUpdate_thenThrowException() {
        AppException e = assertThrows(AppException.class, () -> registerService.update(null));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
        verify(repository, times(0)).save(any());
    }

    @Test
    public void givenValidRegister_whenUpdate_thenRegisterIsUpdated() throws ValidationException {
        Register existingRegister = DomainFactoryUtils.existingRegister();
        existingRegister.setStatus(Status.PENDING);

        when(repository.save(any())).thenReturn(existingRegister);

        RegisterDto registerToUpdate = RegisterMapper.INSTANCE.toRegisterDto(existingRegister);
        RegisterDto registerDto = registerService.update(registerToUpdate);

        verify(repository, times(1)).save(any());
        Truth.assertThat(registerDto).isNotNull();
    }

    @Test
    public void givenEmptyRegister_whenDisable_thenThrowException() {
        ValidationException e = assertThrows(ValidationException.class, () -> registerService.disable(null));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
        verify(repository, times(0)).save(any());
    }

    @Test
    public void givenInvalidRegister_whenDisable_thenThrowException() {
        Register newRegister = DomainFactoryUtils.newRegister();
        when(repository.findByCode(any())).thenReturn(newRegister);
        RegisterDto registerDto = DtoFactoryUtils.registerDto();
        ValidationException e = assertThrows(ValidationException.class, () -> registerService.disable(registerDto.getCode()));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_ID));
        verify(repository, times(0)).save(any());
    }

    @Test
    public void givenValidRegister_whenDisable_thenRegisterIsDisabled() throws ValidationException {
        Register register = DomainFactoryUtils.existingRegister();
        register.setDisabledDate(null);

        when(repository.findByCode(any())).thenReturn(register);
        ArgumentCaptor<Register> captor = ArgumentCaptor.forClass(Register.class);
        registerService.disable(register.getCode());

        verify(repository, times(1)).findByCode(any());
        verify(repository, times(1)).save(captor.capture());

        Register value = captor.getValue();
        Truth.assertThat(value).isNotNull();
        Truth.assertThat(value.isDisabled()).isTrue();
        Truth.assertThat(value.getDisabledDate()).isNotNull();
    }

    @Test
    void givenEmptyRegister_whenEnable_thenThrowException() {
        ValidationException e = assertThrows(ValidationException.class, () -> registerService.enable(null));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
        verify(repository, times(0)).save(any());
    }

    @Test
    void givenInvalidRegister_whenEnable_thenThrowException() {
        Register newRegister = DomainFactoryUtils.newRegister();
        when(repository.findByCode(any())).thenReturn(newRegister);
        RegisterDto registerDto = DtoFactoryUtils.registerDto();
        ValidationException e = assertThrows(ValidationException.class, () -> registerService.enable(registerDto.getCode()));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_ID));
        verify(repository, times(0)).save(any());
    }

    @Test
    void givenValidRegister_whenEnable_thenRegisterIsEnabled() throws ValidationException {
        Register register = DomainFactoryUtils.existingRegister();
        register.setDisabledDate(null);
        register.setDisabled(true);

        when(repository.findByCode(any())).thenReturn(register);
        ArgumentCaptor<Register> captor = ArgumentCaptor.forClass(Register.class);
        registerService.enable(register.getCode());

        verify(repository, times(1)).findByCode(any());
        verify(repository, times(1)).save(captor.capture());

        Register value = captor.getValue();
        Truth.assertThat(value).isNotNull();
        Truth.assertThat(value.isDisabled()).isFalse();
        Truth.assertThat(value.getDisabledDate()).isNull();
    }

    @Test
    void givenNullAccount_whenRegisterAccount_thenThrowsAppException() throws ValidationException, StateException, ApiException {
        AppException e = assertThrows(AppException.class, () -> registerService.registerAccount(null));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, AccountDto.class.getSimpleName()));
        verify(repository, times(0)).findRegisterBy(any(), any());
        verify(registrationContext, times(0)).execute();
        verify(repository, times(0)).save(any());
    }

    @Test
    void givenAccount_whenRegisterExistingAccount_thenAccountIsRegistered() throws ValidationException, StateException, ApiException {
        AccountDto accountDto = DtoFactoryUtils.accountDto();
        Register register = DomainFactoryUtils.existingRegister();

        when(repository.findRegisterBy(any(), any())).thenReturn(null);
        when(repository.save(any())).thenReturn(register);

        RegisterDto registerResponse = assertDoesNotThrow(() -> registerService.registerAccount(accountDto));
        Truth.assertThat(registerResponse).isNotNull();
        verify(repository, times(1)).findRegisterBy(any(), any());
        verify(registrationContext, times(1)).execute();
        verify(repository, times(2)).save(any());
    }

    @Test
    void givenAccount_whenRegisterNewAccount_thenAccountIsRegistered() throws ValidationException, StateException, ApiException {
        AccountDto accountDto = DtoFactoryUtils.accountDto();
        Register register = DomainFactoryUtils.newRegister();
        Register existingRegister = DomainFactoryUtils.existingRegister();

        when(repository.findRegisterBy(any(), any())).thenReturn(register);
        when(repository.save(any())).thenReturn(existingRegister);

        RegisterDto registerResponse = assertDoesNotThrow(() -> registerService.registerAccount(accountDto));
        Truth.assertThat(registerResponse).isNotNull();
        verify(repository, times(1)).findRegisterBy(any(), any());
        verify(registrationContext, times(1)).execute();
        verify(repository, times(2)).save(any());
    }

}