package com.datasoft.cappitalpi.registerservice.integration.service.helper.impl;

import com.datasoft.cappitalpi.registerservice.integration.DomainFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.domain.Status;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.service.helper.RegisterServiceHelper;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RegisterServiceHelperImplTest {

    private RegisterServiceHelper registerServiceHelper;

    @BeforeEach
    void setup() {
        registerServiceHelper = new RegisterServiceHelperImpl();
    }

    @Test
    void givenNullParameters_whenGetPendingRegister_thenReturnNull() {
        Truth.assertThat(registerServiceHelper.getPendingRegister(null, null)).isNull();
    }

    @Test
    void givenNullRegister_whenGetPendingRegister_thenReturnPendingRegister() {
        AccountDto account = DtoFactoryUtils.accountDto();
        RegisterDto pendingRegister = registerServiceHelper.getPendingRegister(null, account);
        Truth.assertThat(pendingRegister).isNotNull();
        Truth.assertThat(pendingRegister.getStatus()).isEqualTo(Status.PENDING.name());
        Truth.assertThat(pendingRegister).isEqualTo(account.getRegister());
    }

    @Test
    void givenNullAccount_whenGetPendingRegister_thenReturnNull() {
        Register register = DomainFactoryUtils.existingRegister();
        RegisterDto pendingRegister = registerServiceHelper.getPendingRegister(register, null);
        Truth.assertThat(pendingRegister).isNull();
    }

    @Test
    void givenValidParameters_whenGetPendingRegister_thenReturnRegister() {
        AccountDto account = DtoFactoryUtils.accountDto();
        Register register = DomainFactoryUtils.existingRegister();
        RegisterDto pendingRegister = registerServiceHelper.getPendingRegister(register, account);
        Truth.assertThat(pendingRegister).isNotNull();
        Truth.assertThat(pendingRegister.getEmail()).isEqualTo(account.getRegister().getEmail());
        Truth.assertThat(pendingRegister.getInscriptionNumber()).isEqualTo(account.getRegister().getInscriptionNumber());
        Truth.assertThat(pendingRegister.getInscriptionTypeId()).isEqualTo(account.getRegister().getInscriptionTypeId());
        Truth.assertThat(pendingRegister.getName()).isEqualTo(account.getRegister().getName());
    }

}