package com.datasoft.cappitalpi.registerservice.integration;

import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupport;
import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupportContext;
import com.datasoft.cappitalpi.core.annotation.support.ColumnLengthAnnotation;
import com.datasoft.cappitalpi.core.annotation.support.ColumnNullableAnnotation;
import com.datasoft.cappitalpi.core.annotation.support.JoinColumnNullableAnnotation;
import com.datasoft.cappitalpi.core.config.CoreConfig;
import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.dto.ErrorResponse;
import com.datasoft.cappitalpi.core.util.UidUtils;
import com.datasoft.cappitalpi.registerservice.integration.config.RegisterConfig;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.ApiResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public final class TestUtils {

    private TestUtils() {
    }

    public static Long newId() {
        int id = new Random().nextInt(Integer.MAX_VALUE);
        return (long) id;
    }

    public static String newUid() {
        return UidUtils.getEncId(newId());
    }

    public static List<String> invalidEmailList() {
        List<String> invalidMails = new ArrayList<>();
        invalidMails.add("aaaa");
        invalidMails.add("aaaa@");
        invalidMails.add("aaaa@a");
        invalidMails.add("aa aa@a.com");
        invalidMails.add("aa..aa@a.com");
        invalidMails.add("aaaa@a.p");
        invalidMails.add("_@_.com");
        invalidMails.add("'@_.com");
        invalidMails.add("=@_.com");
        invalidMails.add(".@.com");
        return invalidMails;
    }

    public static List<String> getInvalidStringValues() {
        List<String> invalidValues = new ArrayList<>();
        invalidValues.add("");
        invalidValues.add("    ");
        invalidValues.add(null);
        return invalidValues;
    }

    public static AnnotationSupportContext getValidatorContext() {
        Set<AnnotationSupport> strategies = new HashSet<>();
        strategies.add(new ColumnLengthAnnotation(messageTranslator()));
        strategies.add(new ColumnNullableAnnotation(messageTranslator()));
        strategies.add(new JoinColumnNullableAnnotation(messageTranslator()));
        return new AnnotationSupportContext(strategies);
    }

    public static MessageSource messageSource() {
        return new RegisterConfig(new CoreConfig()).messageSource();
    }

    public static MessageTranslator messageTranslator() {
        return new MessageTranslator(messageSource());
    }

    public static String formatMessage(String messageId, String... params) {
        MessageTranslator messageTranslator = messageTranslator();
        return messageTranslator.getMessage(messageId, params);
    }

    public static ObjectMapper objectMapper() {
        return new RegisterConfig(new CoreConfig()).objectMapper();
    }

    public static ApiResource apiResource() {
        ApiResource apiResource = new ApiResource();
        apiResource.getAppAccountService().setBaseUrl("app-account-service-base-url");
        apiResource.getAppAccountService().setCreateUrl("create-url");
        apiResource.getAppAccountService().setFindAppAccountByInscriptionUrl("find-app-account-by-inscription-url");
        apiResource.getAppAccountService().setFindInscriptionTypeByUidUrl("find-inscription-type-by-uid-url");
        apiResource.getAppAccountService().setFindAccountByNameUrl("find-account-by-name-url");
        apiResource.getUserService().setBaseUrl("user-service-base-url");
        apiResource.getUserService().setCreateUrl("create-url");
        return apiResource;
    }

    public static ErrorResponse preConditionFailed() {
        ErrorResponse error = new ErrorResponse(HttpStatus.PRECONDITION_REQUIRED.value(), "error message", null);
        error.setBusinessRule(true);
        return error;
    }


}
