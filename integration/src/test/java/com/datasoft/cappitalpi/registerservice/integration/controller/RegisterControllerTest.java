package com.datasoft.cappitalpi.registerservice.integration.controller;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.util.UidUtils;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.RegisterRequestDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;
import com.datasoft.cappitalpi.registerservice.integration.mapper.RegisterMapper;
import com.datasoft.cappitalpi.registerservice.integration.service.RegisterService;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RegisterControllerTest {

    private RegisterController registerController;
    @Mock
    private RegisterService registerService;

    @BeforeEach
    void setUp() {
        registerController = new RegisterController(registerService);
    }

    @Test
    void givenNullCode_whenFindByCode_thenReturn404NotFound() {
        when(registerService.findByCode(any())).thenReturn(null);
        ResponseEntity<RegisterDto> response = registerController.findByCode(10L);

        verify(registerService, times(1)).findByCode(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void givenValidCode_whenFindByCode_thenReturn200() {
        when(registerService.findByCode(any())).thenReturn(new RegisterDto());
        ResponseEntity<RegisterDto> response = registerController.findByCode(10L);

        verify(registerService, times(1)).findByCode(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void givenNullUid_whenFindByUid_thenReturn404NotFound() {
        when(registerService.findByUid(any())).thenReturn(null);
        ResponseEntity<RegisterDto> response = registerController.findByUid("10L");

        verify(registerService, times(1)).findByUid(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void givenValidUid_whenFindByUid_thenReturn200() {
        when(registerService.findByUid(any())).thenReturn(new RegisterDto());
        ResponseEntity<RegisterDto> response = registerController.findByUid("10L");

        verify(registerService, times(1)).findByUid(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void givenNullEmailOrName_whenFindByKeyEnabled_thenReturn404NotFound() {
        when(registerService.findByKey(any())).thenReturn(null);
        ResponseEntity<RegisterDto> response = registerController.findByKey("10L");

        verify(registerService, times(1)).findByKey(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void givenValidEmailOrName_whenFindByEmailOrName_thenReturn200() {
        when(registerService.findByKey(any())).thenReturn(new RegisterDto());
        ResponseEntity<RegisterDto> response = registerController.findByKey("10L");

        verify(registerService, times(1)).findByKey(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void givenNullRegister_whenCreate_thenReturn404NotFound() throws ValidationException, StateException, ApiException {
        HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
        when(registerService.create(any())).thenReturn(null);
        ResponseEntity<RegisterDto> response = registerController.create(null, httpServletRequest);

        verify(registerService, times(1)).registerAccount(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void givenValidRegister_whenCreate_thenReturn200Ok() throws ValidationException, StateException, ApiException {
        HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
        RegisterRequestDto registerRequest = DtoFactoryUtils.registerRequest();
        when(registerService.registerAccount(any())).thenReturn(DtoFactoryUtils.registerDto());
        ResponseEntity<RegisterDto> response = registerController.create(registerRequest, httpServletRequest);

        verify(registerService, times(1)).registerAccount(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void givenNullRegister_whenDisable_thenReturn404NotFound() throws ValidationException {
        when(registerService.disable(any())).thenReturn(null);
        ResponseEntity<RegisterDto> response = registerController.disable(null);

        verify(registerService, times(1)).disable(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void givenValidRegister_whenDisable_thenReturn200Ok() throws ValidationException {
        RegisterDto registerDto = DtoFactoryUtils.registerDto();
        Register register = RegisterMapper.INSTANCE.toRegister(registerDto, UidUtils.getDecId(registerDto.getUid()));
        when(registerService.disable(any())).thenReturn(RegisterMapper.INSTANCE.toRegisterDto(register));
        ResponseEntity<RegisterDto> response = registerController.disable(registerDto.getCode());

        verify(registerService, times(1)).disable(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void givenNullRegister_whenEnable_thenReturn404NotFound() throws ValidationException {
        when(registerService.enable(any())).thenReturn(null);
        ResponseEntity<RegisterDto> response = registerController.enable(null);

        verify(registerService, times(1)).enable(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void givenValidRegister_whenEnable_thenReturn200Ok() throws ValidationException {
        RegisterDto registerDto = DtoFactoryUtils.registerDto();
        Register register = RegisterMapper.INSTANCE.toRegister(registerDto, UidUtils.getDecId(registerDto.getUid()));
        when(registerService.enable(any())).thenReturn(RegisterMapper.INSTANCE.toRegisterDto(register));
        ResponseEntity<RegisterDto> response = registerController.enable(registerDto.getCode());

        verify(registerService, times(1)).enable(any());
        Truth.assertThat(response).isNotNull();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}