package com.datasoft.cappitalpi.registerservice.integration.validator;

import com.datasoft.cappitalpi.core.config.resource.MessageTranslator;
import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.core.validator.AbstractValidationBean;
import com.datasoft.cappitalpi.core.validator.ValidationBean;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
class ValidatorContextTest {

    private ValidatorContext validatorContext;

    @BeforeEach
    void setup() {
        Set<ValidationBean<?>> validators = new HashSet<>();
        validators.add(new TestValidator(TestUtils.messageTranslator()));
        validatorContext = new ValidatorContext(validators);
    }

    @Test
    void givenNullValidators_thenThrowsException() {
        AppException e = assertThrows(AppException.class, () -> new ValidatorContext(null));
        Truth.assertThat(e.getMessage()).isEqualTo("Invalid validator configuration.");
    }

    @Test
    void givenInvalidValidatorType_whenGetInstance_thenReturnValidator() {
        AppException e = assertThrows(AppException.class, () -> validatorContext.instance(Integer.class));
        Truth.assertThat(e.getMessage()).isEqualTo("Invalid validator context configuration.");
    }

    @Test
    void givenValidators_whenGetInstance_thenReturnValidator() {
        ValidationBean<?> instance = validatorContext.instance(String.class);
        Truth.assertThat(instance).isNotNull();
    }

    public static class TestValidator extends AbstractValidationBean<String> {
        protected TestValidator(MessageTranslator messageTranslator) {
            super(messageTranslator);
        }

        @Override
        protected void validateObject(String s) {

        }

        @Override
        public Class<?> type() {
            return String.class;
        }
    }

}