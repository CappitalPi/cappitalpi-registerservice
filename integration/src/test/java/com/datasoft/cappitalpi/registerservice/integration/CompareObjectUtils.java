package com.datasoft.cappitalpi.registerservice.integration;

import com.datasoft.cappitalpi.core.util.UidUtils;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.RegisterRequestDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.UserRequestDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserDto;
import com.google.common.truth.Truth;

public final class CompareObjectUtils {

    private CompareObjectUtils() {
    }

    public static void compare(Register register, RegisterDto registerDto) {
        Truth.assertThat(UidUtils.getEncId(register.getId())).isEqualTo(registerDto.getUid());
        Truth.assertThat(register.getCode()).isEqualTo(registerDto.getCode());
        Truth.assertThat(register.getInscriptionTypeId()).isEqualTo(registerDto.getInscriptionTypeId());
        Truth.assertThat(register.getInscriptionNumber()).isEqualTo(registerDto.getInscriptionNumber());
        Truth.assertThat(register.getName()).isEqualTo(registerDto.getName());
        Truth.assertThat(register.getEmail()).isEqualTo(registerDto.getEmail());
        Truth.assertThat(register.getKey()).isEqualTo(registerDto.getKey());
        Truth.assertThat(register.isDisabled()).isEqualTo(registerDto.isDisabled());
        Truth.assertThat(register.getCreatedDate()).isEqualTo(registerDto.getCreatedDate());
        Truth.assertThat(register.getDisabledDate()).isEqualTo(registerDto.getDisabledDate());
    }

    public static void compare(RegisterRequestDto request, AccountDto account) {
        Truth.assertThat(request.getEmail()).isEqualTo(account.getRegister().getEmail());
        Truth.assertThat(request.getAppAccountRequest().getName()).isEqualTo(account.getRegister().getName());
        Truth.assertThat(request.getAppAccountRequest().getType()).isEqualTo(account.getAppAccount().getType());
        Truth.assertThat(request.getAppAccountRequest().getName()).isEqualTo(account.getAppAccount().getName());
        Truth.assertThat(request.getAppAccountRequest().getBusinessName()).isEqualTo(account.getAppAccount().getBusinessName());
        Truth.assertThat(request.getAppAccountRequest().getEnterpriseType()).isEqualTo(account.getAppAccount().getEnterpriseType());

        Truth.assertThat(request.getInscription().getTypeUid()).isEqualTo(UidUtils.getEncId(account.getRegister().getInscriptionTypeId()));
        Truth.assertThat(request.getInscription().getNumber()).isEqualTo(account.getRegister().getInscriptionNumber());

        compare(request.getUser(), account.getUser());
    }

    private static void compare(UserRequestDto userRequest, UserDto user) {
        Truth.assertThat(userRequest.getName()).isEqualTo(user.getName());
        Truth.assertThat(userRequest.getUsername()).isEqualTo(user.getUsername());
        Truth.assertThat(userRequest.getPassword()).isEqualTo(user.getPassword());
        Truth.assertThat(userRequest.getConfirmPassword()).isEqualTo(user.getConfirmPassword());
    }
}
