package com.datasoft.cappitalpi.registerservice.integration.validator;

import com.datasoft.cappitalpi.core.annotation.support.AnnotationSupportContext;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.DomainFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
class EnableRegisterValidatorTest {

    private EnableRegisterValidator enableRegisterValidator;

    @BeforeEach
    void setUp() {
        AnnotationSupportContext context = TestUtils.getValidatorContext();
        enableRegisterValidator = new EnableRegisterValidator(TestUtils.messageTranslator());
        enableRegisterValidator.setContext(context);
    }

    @Test
    void givenType_thenReturnDisableRegisterValidator() {
        Truth.assertThat(enableRegisterValidator.type()).isEqualTo(EnableRegisterValidator.class);
    }

    @Test
    public void givenNullRegister_whenValidate_thenThrowException() {
        ValidationException e = assertThrows(ValidationException.class, () -> enableRegisterValidator.validate(null));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_OBJECT, Register.class.getSimpleName()));
    }

    @Test
    public void givenNullRegisterId_whenValidate_thenThrowException() {
        Register register = DomainFactoryUtils.newRegister();
        ValidationException e = assertThrows(ValidationException.class, () -> enableRegisterValidator.validate(register));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.MANDATORY_ID));
    }

    @Test
    public void givenEmptyMandatoryField_whenValidate_thenSkipValidations() {
        Register register = DomainFactoryUtils.existingRegister();
        register.setKey(null);
        assertDoesNotThrow(() -> enableRegisterValidator.validate(register));
    }

    @Test
    public void givenValidRegister_whenValidate_thenRegisterIsValidated() {
        Register register = DomainFactoryUtils.existingRegister();
        assertDoesNotThrow(() -> enableRegisterValidator.validate(register));
    }

}