package com.datasoft.cappitalpi.registerservice.integration.service.state;

import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.datasoft.cappitalpi.registerservice.integration.api.AppAccountServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.AppAccountStrategy;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.RegisterStrategy;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.RegisterStrategyContext;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class CreateAppAccountStateTest {

    private CreateAppAccountState createAppAccountState;
    private RegistrationContext registrationContext;
    @Mock
    private AppAccountServiceApi appAccountServiceApi;
    @Mock
    private CreateUserState createUserState;

    @BeforeEach
    void setUp() {
        registrationContext = new RegistrationContext(createAppAccountState);
        Set<RegisterStrategy<?>> strategies = new HashSet<>();
        strategies.add(new AppAccountStrategy(appAccountServiceApi));
        createAppAccountState = new CreateAppAccountState(createUserState, new RegisterStrategyContext(strategies), TestUtils.messageTranslator());
    }

    @Test
    void whenNext_thenReturnCreateUserState() {
        RegistrationState next = createAppAccountState.next();
        Truth.assertThat(next instanceof CreateUserState).isTrue();
    }

    @Test
    void givenInvalidContext_whenExecute_thenThrowsException() throws ApiException {
        registrationContext.setAccountDto(DtoFactoryUtils.accountDto());
        when(appAccountServiceApi.findAppAccountByInscription(any(), any())).thenThrow(NullPointerException.class);
        Exception e = assertThrows(Exception.class, () -> createAppAccountState.execute(registrationContext));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.INTERNAL_ERROR, "null"));
    }

    @Test
    void givenValidContext_whenExecute_thenItIsExecuted() throws ApiException {
        registrationContext.setAccountDto(DtoFactoryUtils.accountDto());
        when(appAccountServiceApi.findAppAccountByInscription(any(), any())).thenReturn(DtoFactoryUtils.appAccountDto());
        assertDoesNotThrow(() -> createAppAccountState.execute(registrationContext));
    }

}