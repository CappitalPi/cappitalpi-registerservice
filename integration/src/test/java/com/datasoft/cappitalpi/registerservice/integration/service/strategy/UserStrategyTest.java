package com.datasoft.cappitalpi.registerservice.integration.service.strategy;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.api.UserServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserResponseDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UserStrategyTest {

    private UserStrategy userStrategy;
    @Mock
    private UserServiceApi userServiceApi;

    @BeforeEach
    void setup() {
        userStrategy = new UserStrategy(userServiceApi);
    }

    @Test
    void whenGetAction_thenReturnCreateUser() {
        Truth.assertThat(userStrategy.action()).isEqualTo(RegisterStrategyAction.CREATE_USER);
    }

    @Test
    void givenNullAccount_whenExecute_thenReturnNull() throws ValidationException, IOException, ApiException {
        UserResponseDto userResponseDto = userStrategy.execute(null);
        Truth.assertThat(userResponseDto).isNull();
        verify(userServiceApi, times(0)).create(any());
    }


    @Test
    void givenNonExistingAccount_whenExecute_thenReturnAppAccount() throws ValidationException, IOException, ApiException {
        when(userServiceApi.create(any())).thenReturn(DtoFactoryUtils.userResponse());

        UserResponseDto userResponseDto = userStrategy.execute(DtoFactoryUtils.accountDto());
        Truth.assertThat(userResponseDto).isNotNull();

        verify(userServiceApi, times(1)).create(any());
    }

}