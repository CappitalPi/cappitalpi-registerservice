package com.datasoft.cappitalpi.registerservice.integration.exception;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class StateExceptionTest {

    @Test
    void givenMessageAndException_whenStateException_thenReturnMessage() {
        String message = "exception message";
        StateException exception = new StateException(message, new Exception());
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
    }

    @Test
    void givenMessageAndException_whenStateException_thenReturnMessageAndCause() {
        String message = "exception message";
        RuntimeException cause = new RuntimeException("any cause");
        StateException exception = new StateException(message, cause);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
        Truth.assertThat(exception.getCause()).isEqualTo(cause);
    }

}