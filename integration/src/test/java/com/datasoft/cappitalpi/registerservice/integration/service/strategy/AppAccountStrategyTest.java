package com.datasoft.cappitalpi.registerservice.integration.service.strategy;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.api.AppAccountServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.AppAccountDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class AppAccountStrategyTest {

    private AppAccountStrategy appAccountStrategy;
    @Mock
    private AppAccountServiceApi appAccountServiceApi;

    @BeforeEach
    void setup() {
        appAccountStrategy = new AppAccountStrategy(appAccountServiceApi);
    }

    @Test
    void whenGetAction_thenReturnCreateAccount() {
        Truth.assertThat(appAccountStrategy.action()).isEqualTo(RegisterStrategyAction.CREATE_ACCOUNT);
    }

    @Test
    void givenNullAccount_whenExecute_thenReturnNull() throws ValidationException, IOException, ApiException {
        AppAccountDto appAccount = appAccountStrategy.execute(null);
        Truth.assertThat(appAccount).isNull();
        verify(appAccountServiceApi, times(0)).findAppAccountByInscription(any(), any());
        verify(appAccountServiceApi, times(0)).findInscriptionTypeByUid(any());
        verify(appAccountServiceApi, times(0)).create(any());
    }

    @Test
    void givenExistingAccount_whenExecute_thenReturnAppAccount() throws ValidationException, IOException, ApiException {
        when(appAccountServiceApi.findAppAccountByInscription(any(), any())).thenReturn(DtoFactoryUtils.appAccountDto());

        AppAccountDto appAccount = appAccountStrategy.execute(DtoFactoryUtils.accountDto());
        Truth.assertThat(appAccount).isNotNull();

        verify(appAccountServiceApi, times(1)).findAppAccountByInscription(any(), any());
        verify(appAccountServiceApi, times(0)).findInscriptionTypeByUid(any());
        verify(appAccountServiceApi, times(0)).create(any());
    }

    @Test
    void givenNonExistingAccount_whenExecute_thenReturnAppAccount() throws ValidationException, IOException, ApiException {
        when(appAccountServiceApi.findAppAccountByInscription(any(), any())).thenReturn(null);
        when(appAccountServiceApi.findInscriptionTypeByUid(any())).thenReturn(DtoFactoryUtils.inscriptionType());
        when(appAccountServiceApi.create(any())).thenReturn(DtoFactoryUtils.appAccountDto());

        AppAccountDto appAccount = appAccountStrategy.execute(DtoFactoryUtils.accountDto());
        Truth.assertThat(appAccount).isNotNull();

        verify(appAccountServiceApi, times(1)).findAppAccountByInscription(any(), any());
        verify(appAccountServiceApi, times(1)).findInscriptionTypeByUid(any());
        verify(appAccountServiceApi, times(1)).create(any());
    }

}