package com.datasoft.cappitalpi.registerservice.integration.repository.impl;

import com.datasoft.cappitalpi.registerservice.integration.DomainFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.repository.RegisterRepositoryCustom;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RegisterRepositoryImplTest {

    private RegisterRepositoryCustom registerRepository;
    @Mock
    private EntityManager entityManager;
    @Mock
    private TypedQuery<Object> query;

    @BeforeEach
    public void setUp() {
        registerRepository = new RegisterRepositoryImpl(entityManager);
    }

    @Test
    void givenNullKey_whenFindByKey_returnEmptyList() {
        Register register = registerRepository.findByKey(null);

        verify(entityManager, times(0)).createQuery(any(), any());
        verify(query, times(0)).getResultList();
        Truth.assertThat(register).isNull();
    }

    @Test
    void givenKey_whenFindByKey_returnEmptyList() {
        when(entityManager.createQuery(any(), any())).thenReturn(query);
        when(query.getResultList()).thenReturn(Collections.emptyList());
        Register register = registerRepository.findByKey("any");

        verify(entityManager, times(1)).createQuery(any(), any());
        verify(query, times(1)).getResultList();
        Truth.assertThat(register).isNull();
    }

    @Test
    void givenKey_whenFindByKey_returnList() {
        when(entityManager.createQuery(any(), any())).thenReturn(query);
        Register register = DomainFactoryUtils.newRegister();
        when(query.getResultList()).thenReturn(Collections.singletonList(register));
        Register result = registerRepository.findByKey("any2");

        verify(entityManager, times(1)).createQuery(any(), any());
        verify(query, times(1)).getResultList();
        Truth.assertThat(result).isNotNull();
    }

    @Test
    void givenNullKey_whenFindRegisterBy_returnEmptyList() {
        Register register = registerRepository.findRegisterBy(null, null);

        verify(entityManager, times(0)).createQuery(any(), any());
        verify(query, times(0)).getResultList();
        Truth.assertThat(register).isNull();
    }

    @Test
    void givenKey_whenFindRegisterBy_returnEmptyList() {
        when(entityManager.createQuery(any(), any())).thenReturn(query);
        when(query.getResultList()).thenReturn(Collections.emptyList());
        Register register = registerRepository.findRegisterBy("any", "any2");

        verify(entityManager, times(1)).createQuery(any(), any());
        verify(query, times(1)).getResultList();
        Truth.assertThat(register).isNull();
    }

    @Test
    void givenKey_whenFindRegisterBy_returnList() {
        when(entityManager.createQuery(any(), any())).thenReturn(query);
        Register register = DomainFactoryUtils.newRegister();
        when(query.getResultList()).thenReturn(Collections.singletonList(register));
        Register result = registerRepository.findRegisterBy("any", "any2");

        verify(entityManager, times(1)).createQuery(any(), any());
        verify(query, times(1)).getResultList();
        Truth.assertThat(result).isNotNull();
    }

}