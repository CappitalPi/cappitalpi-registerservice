package com.datasoft.cappitalpi.registerservice.integration.service.state;

import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class RegistrationContextTest {

    private RegistrationContext registrationContext;
    @Mock
    private CreateAppAccountState createAppAccountState;

    @BeforeEach
    void setup() {
        registrationContext = new RegistrationContext(createAppAccountState);
    }

    @Test
    void givenNullAccount_thenInitialized_thenAccountIsNull() {
        registrationContext.initialize(null);
        Truth.assertThat(registrationContext.getAccountDto()).isNull();
    }

    @Test
    void givenAccount_thenInitialized_thenAccountIsNull() {
        registrationContext.initialize(DtoFactoryUtils.accountDto());
        Truth.assertThat(registrationContext.getAccountDto()).isNotNull();
    }

    @Test
    void givenNullState_thenExecute_thenThrowsException() {
        registrationContext = new RegistrationContext(null);
        AppException e = Assertions.assertThrows(AppException.class, () -> registrationContext.execute());
        Truth.assertThat(e.getMessage()).isEqualTo("State context not initialized");
    }

    @Test
    void givenValidState_thenExecute_thenIsExecuted() {
        registrationContext.initialize(DtoFactoryUtils.accountDto());
        Assertions.assertDoesNotThrow(() -> registrationContext.execute());
    }

    @Test
    void givenValidState_thenExecute_thenIsExecutedTwoTimes() {
        Mockito.when(createAppAccountState.next()).thenReturn(Mockito.mock(CreateUserState.class));
        registrationContext.initialize(DtoFactoryUtils.accountDto());
        Assertions.assertDoesNotThrow(() -> registrationContext.execute());
    }

}