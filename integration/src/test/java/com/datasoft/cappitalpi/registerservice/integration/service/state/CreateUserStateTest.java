package com.datasoft.cappitalpi.registerservice.integration.service.state;

import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.datasoft.cappitalpi.registerservice.integration.api.UserServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.datasoft.cappitalpi.registerservice.integration.exception.StateException;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.RegisterStrategy;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.RegisterStrategyContext;
import com.datasoft.cappitalpi.registerservice.integration.service.strategy.UserStrategy;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class CreateUserStateTest {

    @Mock
    private CreateAppAccountState createAppAccountState;
    private CreateUserState createUserState;
    private RegistrationContext registrationContext;
    @Mock
    private UserServiceApi userServiceApi;

    @BeforeEach
    void setUp() {
        registrationContext = new RegistrationContext(createAppAccountState);
        registrationContext.setAccountDto(DtoFactoryUtils.accountDto());
        Set<RegisterStrategy<?>> strategies = new HashSet<>();
        strategies.add(new UserStrategy(userServiceApi));
        createUserState = new CreateUserState(new RegisterStrategyContext(strategies), TestUtils.messageTranslator());
    }

    @Test
    void givenInvalidContext_whenExecute_thenThrowsException() throws ApiException, ValidationException, IOException {
        registrationContext.setAccountDto(DtoFactoryUtils.accountDto());
        when(userServiceApi.create(any())).thenThrow(IOException.class);
        StateException e = assertThrows(StateException.class, () -> createUserState.execute(registrationContext));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.INTERNAL_ERROR, "null"));
    }

    @Test
    void givenValidContext_whenExecute_thenItIsExecuted() throws ApiException, ValidationException, IOException {
        registrationContext.setAccountDto(DtoFactoryUtils.accountDto());
        when(userServiceApi.create(any())).thenReturn(DtoFactoryUtils.userResponse());
        assertDoesNotThrow(() -> createUserState.execute(registrationContext));
        RegistrationState next = createUserState.next();
        Truth.assertThat(next).isNull();
    }

}