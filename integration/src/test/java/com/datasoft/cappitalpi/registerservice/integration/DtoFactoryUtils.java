package com.datasoft.cappitalpi.registerservice.integration;

import com.datasoft.cappitalpi.registerservice.integration.domain.Status;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.AppAccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.InscriptionTypeDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.AppAccountRequestDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.InscriptionRequestDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.RegisterRequestDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.UserRequestDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserResponseDto;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;
import java.util.UUID;

public class DtoFactoryUtils {

    public static RegisterRequestDto registerRequest() {
        RegisterRequestDto request = new RegisterRequestDto();
        request.setEmail("a@a.com");
        request.setAppAccountRequest(appAccountRequest());
        request.setInscription(inscriptionRequest());
        request.setUser(userRequest());
        return request;

    }

    private static UserRequestDto userRequest() {
        UserRequestDto user = new UserRequestDto();
        user.setName(RandomStringUtils.randomAlphabetic(30));
        user.setUsername(RandomStringUtils.randomAlphabetic(10));
        user.setPassword(RandomStringUtils.randomAlphabetic(255));
        user.setConfirmPassword(RandomStringUtils.randomAlphabetic(255));
        return user;
    }

    private static InscriptionRequestDto inscriptionRequest() {
        InscriptionRequestDto request = new InscriptionRequestDto();
        request.setNumber(TestUtils.newUid());
        request.setTypeUid(TestUtils.newUid());
        return request;
    }

    private static AppAccountRequestDto appAccountRequest() {
        AppAccountRequestDto request = new AppAccountRequestDto();
        request.setBusinessName(RandomStringUtils.randomAlphabetic(30));
        request.setName(RandomStringUtils.randomAlphabetic(30));
        request.setType(RandomStringUtils.randomAlphabetic(1));
        request.setEnterpriseType(RandomStringUtils.randomAlphabetic(1));
        return request;
    }

    public static RegisterDto registerDto() {
        RegisterDto registerDto = new RegisterDto();
        registerDto.setUid(TestUtils.newUid());
        registerDto.setCode(TestUtils.newId());
        registerDto.setInscriptionTypeId(TestUtils.newId());
        registerDto.setInscriptionNumber(RandomStringUtils.randomAlphabetic(30));
        registerDto.setKey(UUID.randomUUID().toString());
        registerDto.setName(RandomStringUtils.randomAlphabetic(80));
        registerDto.setEmail("RegisterDto@email.com");
        registerDto.setIp("127.0.0.1");
        registerDto.setStatus(Status.COMPLETED.name());
        registerDto.setDisabled(true);
        registerDto.setCreatedDate(LocalDateTime.now());
        return registerDto;
    }

    public static AppAccountDto appAccountDto() {
        AppAccountDto appAccount = new AppAccountDto();
        appAccount.setUid(TestUtils.newUid());
        appAccount.setCode(TestUtils.newId());
        appAccount.setRegisterUid(TestUtils.newUid());
        appAccount.setType("L");
        appAccount.setName(RandomStringUtils.randomAlphabetic(30));
        appAccount.setBusinessName(RandomStringUtils.randomAlphabetic(30));
        appAccount.setEnterpriseType(RandomStringUtils.randomAlphabetic(1));
        appAccount.setImagePath(RandomStringUtils.randomAlphabetic(255));
        appAccount.setDisabled(false);
        appAccount.setCreatedDate(LocalDateTime.now());
        appAccount.setUpdatedDate(LocalDateTime.now());
        return appAccount;
    }

    public static InscriptionTypeDto inscriptionType() {
        InscriptionTypeDto type = new InscriptionTypeDto();
        type.setUid(TestUtils.newUid());
        type.setCode(TestUtils.newId());
        type.setName(RandomStringUtils.randomAlphabetic(80));
        return type;
    }

    public static UserResponseDto userResponse() {
        UserResponseDto user = new UserResponseDto();
        user.setUid(TestUtils.newUid());
        user.setCode(TestUtils.newId());
        user.setName(RandomStringUtils.randomAlphabetic(30));
        user.setEmail("a@a.com");
        user.setUsername(RandomStringUtils.randomAlphabetic(10));
        user.setDisabled(false);
        user.setAdmin(true);
        user.setCreatedDate(LocalDateTime.now());
        user.setUpdatedDate(LocalDateTime.now());
        user.setDisabledDate(null);
        return user;
    }

    public static UserDto user() {
        UserDto user = new UserDto();
        user.setUid(TestUtils.newUid());
        user.setRegisterUid(TestUtils.newUid());
        user.setCode(TestUtils.newId());
        user.setName(RandomStringUtils.randomAlphabetic(30));
        user.setEmail("a@a.com");
        user.setUsername(RandomStringUtils.randomAlphabetic(10));
        user.setPassword(RandomStringUtils.randomAlphabetic(255));
        user.setConfirmPassword(RandomStringUtils.randomAlphabetic(255));
        return user;
    }

    public static AccountDto accountDto() {
        return new AccountDto.Builder()
                .withRegister(registerDto())
                .withAccount(appAccountDto())
                .withUser(user())
                .build();
    }
}
