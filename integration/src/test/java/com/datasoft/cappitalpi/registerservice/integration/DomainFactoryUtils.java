package com.datasoft.cappitalpi.registerservice.integration;

import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.domain.Status;
import org.apache.commons.lang3.RandomStringUtils;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.UUID;

public class DomainFactoryUtils {

    public static Register newRegister() {
        Register register = new Register();
        register.setCode(TestUtils.newId());
        register.setInscriptionTypeId(TestUtils.newId());
        register.setName(RandomStringUtils.randomAlphabetic(80));
        register.setInscriptionNumber(RandomStringUtils.randomAlphabetic(30));
        register.setKey(UUID.nameUUIDFromBytes(register.getCode().toString().getBytes(StandardCharsets.UTF_8)).toString());
        register.setEmail("new-register@email.com");
        register.setIp("127.0.0.1");
        register.setStatus(Status.PENDING);
        register.setDisabled(false);
        register.setCreatedDate(LocalDateTime.now());
        return register;
    }

    public static Register existingRegister() {
        Register register = new Register();
        register.setId(TestUtils.newId());
        register.setCode(TestUtils.newId());
        register.setInscriptionTypeId(TestUtils.newId());
        register.setName(RandomStringUtils.randomAlphabetic(80));
        register.setInscriptionNumber(RandomStringUtils.randomAlphabetic(30));
        register.setKey(UUID.randomUUID().toString());
        register.setEmail("new-register@email.com");
        register.setIp("127.0.0.1");
        register.setStatus(Status.COMPLETED);
        register.setDisabled(false);
        register.setCreatedDate(LocalDateTime.now());
        return register;
    }

}
