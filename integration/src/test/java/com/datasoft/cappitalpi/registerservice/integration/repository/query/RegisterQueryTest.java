package com.datasoft.cappitalpi.registerservice.integration.repository.query;

import com.datasoft.cappitalpi.core.repository.QueryRequest;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class RegisterQueryTest {

    @Test
    void givenParameters_whenFindByKey_thenReturnNull() {
        for (String invalidStringValue : TestUtils.getInvalidStringValues()) {
            Truth.assertThat(RegisterQuery.findByKey(invalidStringValue)).isNull();
        }
    }

    @Test
    void givenParameters_whenFindByKey_thenReturnQuery() {
        QueryRequest queryRequest = RegisterQuery.findByKey("any");
        Truth.assertThat(queryRequest).isNotNull();
        Truth.assertThat(queryRequest.getParams()).isNotNull();
        Truth.assertThat(queryRequest.getParams()).isNotEmpty();
        Truth.assertThat(queryRequest.getParams().size()).isEqualTo(1);
        Truth.assertThat(queryRequest.getQuery()).isNotEmpty();
        Truth.assertThat(queryRequest.getQuery()).isNotEmpty();
    }

    @Test
    void givenParameters_whenFindRegisterBy_thenReturnNull() {
        for (String invalidStringValue : TestUtils.getInvalidStringValues()) {
            Truth.assertThat(RegisterQuery.findRegisterBy(invalidStringValue, invalidStringValue)).isNull();
        }
    }

    @Test
    void givenParameters_whenFindRegisterBy_thenReturnQuery() {
        QueryRequest queryRequest = RegisterQuery.findRegisterBy("any", "any");
        Truth.assertThat(queryRequest).isNotNull();
        Truth.assertThat(queryRequest.getParams()).isNotNull();
        Truth.assertThat(queryRequest.getParams()).isNotEmpty();
        Truth.assertThat(queryRequest.getParams().size()).isEqualTo(2);
        Truth.assertThat(queryRequest.getQuery()).isNotEmpty();
        Truth.assertThat(queryRequest.getQuery()).isNotEmpty();
    }

}