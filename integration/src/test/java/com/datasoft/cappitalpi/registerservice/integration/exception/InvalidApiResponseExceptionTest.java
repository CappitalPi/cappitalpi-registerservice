package com.datasoft.cappitalpi.registerservice.integration.exception;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class InvalidApiResponseExceptionTest {

    @Test
    void givenMessage_whenInvalidApiResponseException_thenReturnMessage() {
        String message = "exception message";
        InvalidApiResponseException exception = new InvalidApiResponseException(message);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
    }

}