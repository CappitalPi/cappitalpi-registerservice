package com.datasoft.cappitalpi.registerservice.integration.api.impl;

import com.datasoft.cappitalpi.core.dto.ErrorResponse;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.util.HeaderConstant;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.datasoft.cappitalpi.registerservice.integration.api.UserServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.ApiResource;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.user.UserResponseDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UserServiceApiImplTest {

    private UserServiceApi userServiceApi;
    @Mock
    private RestTemplate restTemplate;
    private ApiResource apiResource;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        apiResource = TestUtils.apiResource();
        objectMapper = TestUtils.objectMapper();
        userServiceApi = new UserServiceApiImpl(restTemplate, apiResource, TestUtils.messageTranslator(), objectMapper);
    }

    @Test
    void givenNullAppAccount_whenCreate_thenReturnNull() throws ValidationException, IOException, ApiException {
        Truth.assertThat(userServiceApi.create(null)).isNull();
    }

    @Test
    void givenValidParameters_whenCreate_thenThrowsValidationException() throws JsonProcessingException {
        UserDto body = DtoFactoryUtils.user();
        ErrorResponse error = TestUtils.preConditionFailed();
        String errorMessage = objectMapper.writeValueAsString(error);
        when(restTemplate.exchange(
                eq(apiResource.getUserService().getCreateUrl()),
                eq(HttpMethod.POST),
                any(),
                eq(UserResponseDto.class))
        ).thenThrow(new HttpClientErrorException(HttpStatus.PRECONDITION_REQUIRED, "any", errorMessage.getBytes(), StandardCharsets.UTF_8));
        ValidationException e = assertThrows(ValidationException.class, () -> userServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isEqualTo(error.getMessage());
    }

    @Test
    void givenValidParameters_whenCreate_thenNotPreConditionFailedAndThrowsHttpClientErrorException() throws JsonProcessingException {
        UserDto body = DtoFactoryUtils.user();
        ErrorResponse error = TestUtils.preConditionFailed();
        String errorMessage = objectMapper.writeValueAsString(error);
        when(restTemplate.exchange(
                eq(apiResource.getUserService().getCreateUrl()),
                eq(HttpMethod.POST),
                any(),
                eq(UserResponseDto.class))
        ).thenThrow(new HttpClientErrorException(HttpStatus.FORBIDDEN, "any", errorMessage.getBytes(), StandardCharsets.UTF_8));
        HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> userServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isNotEqualTo(error.getMessage());
    }

    @Test
    void givenValidParameters_whenCreate_thenThrowsRestClientException() {
        UserDto body = DtoFactoryUtils.user();
        when(restTemplate.exchange(
                eq(apiResource.getUserService().getCreateUrl()),
                eq(HttpMethod.POST),
                any(),
                eq(UserResponseDto.class))
        ).thenThrow(RestClientException.class);
        ApiException e = assertThrows(ApiException.class, () -> userServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getUserService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenCreate_thenThrowsException() {
        UserDto body = DtoFactoryUtils.user();
        when(restTemplate.exchange(
                eq(apiResource.getUserService().getCreateUrl()),
                eq(HttpMethod.POST),
                any(),
                eq(UserResponseDto.class))
        ).thenThrow(NullPointerException.class);
        ApiException e = assertThrows(ApiException.class, () -> userServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getUserService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenCreate_thenItIsSent() {
        UserDto body = DtoFactoryUtils.user();
        ResponseEntity<UserResponseDto> response = new ResponseEntity<>(DtoFactoryUtils.userResponse(), HttpStatus.CREATED);
        var captor = ArgumentCaptor.forClass(HttpEntity.class);
        when(restTemplate.exchange(eq(apiResource.getUserService().getCreateUrl()), eq(HttpMethod.POST), captor.capture(), eq(UserResponseDto.class))).thenReturn(response);
        UserResponseDto userResponseDto = assertDoesNotThrow(() -> userServiceApi.create(body));
        Truth.assertThat(userResponseDto).isNotNull();
        var httpEntity = captor.getValue();
        Truth.assertThat(httpEntity.getHeaders().get(HeaderConstant.REGISTER_UID)).isNotEmpty();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void givenValidParameters_whenCreate_thenReturnInvalidResponse() {
        UserDto body = DtoFactoryUtils.user();
        ResponseEntity<UserResponseDto> response = new ResponseEntity<>(DtoFactoryUtils.userResponse(), HttpStatus.OK);
        when(restTemplate.exchange(eq(apiResource.getUserService().getCreateUrl()), eq(HttpMethod.POST), any(), eq(UserResponseDto.class))).thenReturn(response);
        ApiException e = assertThrows(ApiException.class, () -> userServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getUserService().getBaseUrl()));
        Truth.assertThat(e.getCause().getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.INVALID_API_RESPONSE, apiResource.getUserService().getBaseUrl()));
    }

}