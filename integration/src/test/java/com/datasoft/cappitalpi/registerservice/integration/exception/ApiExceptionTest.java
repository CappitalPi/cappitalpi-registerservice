package com.datasoft.cappitalpi.registerservice.integration.exception;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class ApiExceptionTest {

    @Test
    void givenMessageAndException_whenApiException_thenReturnMessage() {
        String message = "exception message";
        ApiException exception = new ApiException(message, new Exception());
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
    }

    @Test
    void givenMessageAndException_whenApiException_thenReturnMessageAndCause() {
        String message = "exception message";
        RuntimeException cause = new RuntimeException("any cause");
        ApiException exception = new ApiException(message, cause);
        Truth.assertThat(exception.getMessage()).isEqualTo(message);
        Truth.assertThat(exception.getCause()).isEqualTo(cause);
    }

}