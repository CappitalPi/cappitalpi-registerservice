package com.datasoft.cappitalpi.registerservice.integration.api.impl;

import com.datasoft.cappitalpi.core.dto.ErrorResponse;
import com.datasoft.cappitalpi.core.exception.ValidationException;
import com.datasoft.cappitalpi.core.util.HeaderConstant;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.datasoft.cappitalpi.registerservice.integration.api.AppAccountServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.ApiResource;
import com.datasoft.cappitalpi.registerservice.integration.config.resource.MessageIdResource;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.AppAccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.appaccount.InscriptionTypeDto;
import com.datasoft.cappitalpi.registerservice.integration.exception.ApiException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class AppAccountServiceApiImplTest {

    private AppAccountServiceApi appAccountServiceApi;
    @Mock
    private RestTemplate restTemplate;
    private ApiResource apiResource;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = TestUtils.objectMapper();
        apiResource = TestUtils.apiResource();
        appAccountServiceApi = new AppAccountServiceApiImpl(restTemplate, apiResource, TestUtils.messageTranslator());
    }

    @Test
    void givenNullAppAccount_whenCreate_thenReturnNull() throws ValidationException, IOException, ApiException {
        Truth.assertThat(appAccountServiceApi.create(null)).isNull();
    }

    @Test
    void givenValidParameters_whenCreate_thenThrowsValidationException() throws JsonProcessingException {
        AppAccountDto body = DtoFactoryUtils.appAccountDto();
        ErrorResponse error = TestUtils.preConditionFailed();
        String errorMessage = objectMapper.writeValueAsString(error);
        when(restTemplate.exchange(
                eq(apiResource.getAppAccountService().getCreateUrl()),
                eq(HttpMethod.POST),
                any(),
                eq(AppAccountDto.class))
        ).thenThrow(new HttpClientErrorException(HttpStatus.PRECONDITION_REQUIRED, "any", errorMessage.getBytes(), StandardCharsets.UTF_8));
        ValidationException e = assertThrows(ValidationException.class, () -> appAccountServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isEqualTo(error.getMessage());
    }

    @Test
    void givenValidParameters_whenCreate_thenNotPreConditionFailedAndThrowsHttpClientErrorException() throws JsonProcessingException {
        AppAccountDto body = DtoFactoryUtils.appAccountDto();
        ErrorResponse error = TestUtils.preConditionFailed();
        String errorMessage = objectMapper.writeValueAsString(error);
        when(restTemplate.exchange(
                eq(apiResource.getAppAccountService().getCreateUrl()),
                eq(HttpMethod.POST),
                any(),
                eq(AppAccountDto.class))
        ).thenThrow(new HttpClientErrorException(HttpStatus.FORBIDDEN, "any", errorMessage.getBytes(), StandardCharsets.UTF_8));
        HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> appAccountServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isNotEqualTo(error.getMessage());
    }

    @Test
    void givenValidParameters_whenCreate_thenThrowsRestClientException() {
        AppAccountDto body = DtoFactoryUtils.appAccountDto();
        when(restTemplate.exchange(
                eq(apiResource.getAppAccountService().getCreateUrl()),
                eq(HttpMethod.POST),
                any(),
                eq(AppAccountDto.class))
        ).thenThrow(RestClientException.class);
        ApiException e = assertThrows(ApiException.class, () -> appAccountServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenCreate_thenThrowsException() {
        AppAccountDto body = DtoFactoryUtils.appAccountDto();
        when(restTemplate.exchange(
                eq(apiResource.getAppAccountService().getCreateUrl()),
                eq(HttpMethod.POST),
                any(),
                eq(AppAccountDto.class))
        ).thenThrow(NullPointerException.class);
        ApiException e = assertThrows(ApiException.class, () -> appAccountServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenCreate_thenItIsSent() {
        AppAccountDto body = DtoFactoryUtils.appAccountDto();
        ResponseEntity<AppAccountDto> response = new ResponseEntity<>(body, HttpStatus.CREATED);
        var captor = ArgumentCaptor.forClass(HttpEntity.class);
        when(restTemplate.exchange(eq(apiResource.getAppAccountService().getCreateUrl()), eq(HttpMethod.POST), captor.capture(), eq(AppAccountDto.class))).thenReturn(response);
        AppAccountDto appAccountDto = assertDoesNotThrow(() -> appAccountServiceApi.create(body));
        Truth.assertThat(appAccountDto).isNotNull();
        var httpEntity = captor.getValue();
        Truth.assertThat(httpEntity.getHeaders().get(HeaderConstant.OWNER_UID)).isNotEmpty();
        Truth.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void givenValidParameters_whenCreate_thenReturnInvalidResponse() {
        AppAccountDto body = DtoFactoryUtils.appAccountDto();
        ResponseEntity<AppAccountDto> response = new ResponseEntity<>(body, HttpStatus.OK);
        when(restTemplate.exchange(eq(apiResource.getAppAccountService().getCreateUrl()), eq(HttpMethod.POST), any(), eq(AppAccountDto.class))).thenReturn(response);
        ApiException e = assertThrows(ApiException.class, () -> appAccountServiceApi.create(body));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()));
        Truth.assertThat(e.getCause().getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.INVALID_API_RESPONSE, apiResource.getAppAccountService().getBaseUrl()));
    }

    @Test
    void givenNullParameters_whenFindInscriptionTypeByUid_thenReturnNull() throws ApiException {
        Truth.assertThat(appAccountServiceApi.findInscriptionTypeByUid(null)).isNull();
    }

    @Test
    void givenValidParameters_whenFindInscriptionTypeByUid_thenReturnNotFound() throws ApiException {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindInscriptionTypeByUidUrl(), uid);
        ResponseEntity<InscriptionTypeDto> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(restTemplate.getForEntity(eq(endPoint), eq(InscriptionTypeDto.class))).thenReturn(response);
        InscriptionTypeDto inscriptionTypeDto = appAccountServiceApi.findInscriptionTypeByUid(uid);
        Truth.assertThat(inscriptionTypeDto).isNull();
    }

    @Test
    void givenValidParameters_whenFindInscriptionTypeByUid_thenThrowsRestClientException() {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindInscriptionTypeByUidUrl(), uid);

        when(restTemplate.getForEntity(eq(endPoint), eq(InscriptionTypeDto.class))).thenThrow(RestClientException.class);
        ApiException e = assertThrows(ApiException.class, () -> appAccountServiceApi.findInscriptionTypeByUid(uid));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenFindInscriptionTypeByUid_thenThrowsException() {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindInscriptionTypeByUidUrl(), uid);

        when(restTemplate.getForEntity(eq(endPoint), eq(InscriptionTypeDto.class))).thenThrow(NullPointerException.class);
        ApiException e = assertThrows(ApiException.class, () -> appAccountServiceApi.findInscriptionTypeByUid(uid));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenFindInscriptionTypeByUid_thenReturnInscriptionType() throws ApiException {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindInscriptionTypeByUidUrl(), uid);

        ResponseEntity<InscriptionTypeDto> response = ResponseEntity.ok(DtoFactoryUtils.inscriptionType());
        when(restTemplate.getForEntity(eq(endPoint), eq(InscriptionTypeDto.class))).thenReturn(response);

        var inscriptionType = appAccountServiceApi.findInscriptionTypeByUid(uid);
        Truth.assertThat(inscriptionType).isNotNull();
    }

    @Test
    void givenNullParameters_whenFindAppAccountByInscription_thenReturnNull() throws ApiException {
        Truth.assertThat(appAccountServiceApi.findAppAccountByInscription(null, null)).isNull();
    }

    @Test
    void givenValidParameters_whenFindAppAccountByInscription_thenReturnNotFound() throws ApiException {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindAppAccountByInscriptionUrl(), uid);
        ResponseEntity<AppAccountDto> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(restTemplate.getForEntity(eq(endPoint), eq(AppAccountDto.class))).thenReturn(response);
        var appAccount = appAccountServiceApi.findAppAccountByInscription(uid, TestUtils.newId());
        Truth.assertThat(appAccount).isNull();
    }

    @Test
    void givenValidParameters_whenFindAppAccountByInscription_thenThrowsRestClientException() {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindAppAccountByInscriptionUrl(), uid);

        when(restTemplate.getForEntity(eq(endPoint), eq(AppAccountDto.class))).thenThrow(RestClientException.class);
        ApiException e = assertThrows(ApiException.class, () -> appAccountServiceApi.findAppAccountByInscription(uid, TestUtils.newId()));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenFindAppAccountByInscription_thenThrowsException() {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindAppAccountByInscriptionUrl(), uid);

        when(restTemplate.getForEntity(eq(endPoint), eq(AppAccountDto.class))).thenThrow(NullPointerException.class);
        ApiException e = assertThrows(ApiException.class, () -> appAccountServiceApi.findAppAccountByInscription(uid, TestUtils.newId()));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenFindAppAccountByInscription_thenReturnAppAccount() throws ApiException {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindAppAccountByInscriptionUrl(), uid);

        ResponseEntity<AppAccountDto> response = ResponseEntity.ok(DtoFactoryUtils.appAccountDto());
        when(restTemplate.getForEntity(eq(endPoint), eq(AppAccountDto.class))).thenReturn(response);

        var appAccountDto = appAccountServiceApi.findAppAccountByInscription(uid, TestUtils.newId());
        Truth.assertThat(appAccountDto).isNotNull();
    }

    @Test
    void givenNullParameters_whenFindAccountByName_thenReturnNull() throws ApiException {
        Truth.assertThat(appAccountServiceApi.findAccountByName(null, null)).isNull();
    }

    @Test
    void givenValidParameters_whenFindAccountByName_thenReturnNotFound() throws ApiException {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindAccountByNameUrl(), uid);
        ResponseEntity<AppAccountDto> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(restTemplate.getForEntity(eq(endPoint), eq(AppAccountDto.class))).thenReturn(response);
        var appAccount = appAccountServiceApi.findAccountByName(uid, TestUtils.newId());
        Truth.assertThat(appAccount).isNull();
    }

    @Test
    void givenValidParameters_whenFindAccountByName_thenThrowsRestClientException() {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindAccountByNameUrl(), uid);

        when(restTemplate.getForEntity(eq(endPoint), eq(AppAccountDto.class))).thenThrow(RestClientException.class);
        ApiException e = assertThrows(ApiException.class, () -> appAccountServiceApi.findAccountByName(uid, TestUtils.newId()));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.SOMETHING_WENT_WRONG_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenFindAccountByName_thenThrowsException() {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindAccountByNameUrl(), uid);

        when(restTemplate.getForEntity(eq(endPoint), eq(AppAccountDto.class))).thenThrow(NullPointerException.class);
        ApiException e = assertThrows(ApiException.class, () -> appAccountServiceApi.findAccountByName(uid, TestUtils.newId()));
        Truth.assertThat(e.getMessage()).isEqualTo(TestUtils.formatMessage(MessageIdResource.Validation.ERROR_CALLING_SERVICE, apiResource.getAppAccountService().getBaseUrl()));
    }

    @Test
    void givenValidParameters_whenFindAccountByName_thenReturnAppAccount() throws ApiException {
        final String uid = TestUtils.newUid();
        final String endPoint = MessageFormat.format(apiResource.getAppAccountService().getFindAccountByNameUrl(), uid);

        ResponseEntity<AppAccountDto> response = ResponseEntity.ok(DtoFactoryUtils.appAccountDto());
        when(restTemplate.getForEntity(eq(endPoint), eq(AppAccountDto.class))).thenReturn(response);

        var appAccountDto = appAccountServiceApi.findAccountByName(uid, TestUtils.newId());
        Truth.assertThat(appAccountDto).isNotNull();
    }

}