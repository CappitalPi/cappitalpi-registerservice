package com.datasoft.cappitalpi.registerservice.integration.service.strategy;

import com.datasoft.cappitalpi.core.exception.AppException;
import com.datasoft.cappitalpi.registerservice.integration.api.AppAccountServiceApi;
import com.datasoft.cappitalpi.registerservice.integration.api.UserServiceApi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
class RegisterStrategyContextTest {

    private RegisterStrategyContext context;

    @Mock
    private UserServiceApi userServiceApi;
    @Mock
    private AppAccountServiceApi appAccountServiceApi;

    @BeforeEach
    public void setUp() {
        Set<RegisterStrategy<?>> strategies = new HashSet<>();
        strategies.add(new UserStrategy(userServiceApi));
        strategies.add(new AppAccountStrategy(appAccountServiceApi));
        context = new RegisterStrategyContext(strategies);
    }

    @Test
    void givenInvalidRegisters_whenRegister_thenThrowException() {
        assertThrows(AppException.class, () -> new RegisterStrategyContext(null));
    }

    @Test
    void givenNullAction_whenGetContext_thenThrowException() {
        assertThrows(AppException.class, () -> context.context(null));
    }

    @Test
    void givenAction_whenGetContext_thenReturnContext() {
        for (RegisterStrategyAction value : RegisterStrategyAction.values()) {
            assertDoesNotThrow(() -> context.context(value));
        }
    }

}