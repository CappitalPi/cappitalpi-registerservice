package com.datasoft.cappitalpi.registerservice.integration.mapper;

import com.datasoft.cappitalpi.core.util.UidUtils;
import com.datasoft.cappitalpi.registerservice.integration.CompareObjectUtils;
import com.datasoft.cappitalpi.registerservice.integration.DomainFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.DtoFactoryUtils;
import com.datasoft.cappitalpi.registerservice.integration.TestUtils;
import com.datasoft.cappitalpi.registerservice.integration.domain.Register;
import com.datasoft.cappitalpi.registerservice.integration.dto.AccountDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.RegisterDto;
import com.datasoft.cappitalpi.registerservice.integration.dto.request.RegisterRequestDto;
import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

class RegisterMapperTest {

    @Test
    void givenNullRegister_whenMapToDto_thenReturnNull() {
        RegisterDto registerDto = RegisterMapper.INSTANCE.toRegisterDto(null);
        Truth.assertThat(registerDto).isNull();
    }

    @Test
    void givenValidRegister_whenMapToDto_thenReturnDto() {
        Register entity = DomainFactoryUtils.existingRegister();
        RegisterDto registerDto = RegisterMapper.INSTANCE.toRegisterDto(entity);
        Truth.assertThat(registerDto).isNotNull();
        Truth.assertThat(registerDto.getUid()).isNotNull();

        CompareObjectUtils.compare(entity, registerDto);
    }

    @Test
    void givenNullRegister_whenMapToEntity_thenReturnNull() {
        Register register = RegisterMapper.INSTANCE.toRegister(null, null);
        Truth.assertThat(register).isNull();
    }

    @Test
    void givenValidRegisterDto_whenMapToEntity_thenReturnEntity() {
        RegisterDto dto = DtoFactoryUtils.registerDto();
        Register register = RegisterMapper.INSTANCE.toRegister(dto, UidUtils.getDecId(dto.getUid()));
        Truth.assertThat(register).isNotNull();
        Truth.assertThat(register.getUid()).isNotNull();
        Truth.assertThat(register.getCode()).isNotNull();
        Truth.assertThat(register.getName()).isNotNull();
        Truth.assertThat(register.getEmail()).isNotNull();
        Truth.assertThat(register.getInscriptionTypeId()).isNotNull();
        Truth.assertThat(register.getInscriptionNumber()).isNotNull();
        Truth.assertThat(register.getKey()).isNotNull();
        Truth.assertThat(register.getCreatedDate()).isNotNull();
        Truth.assertThat(register.isDisabled()).isNotNull();
        CompareObjectUtils.compare(register, dto);
    }

    @Test
    void givenValidRegisterDto_whenMapToDomain_thenReturnDto() {
        RegisterDto dto = DtoFactoryUtils.registerDto();
        Register register = RegisterMapper.INSTANCE.toRegister(dto, UidUtils.getDecId(dto.getUid()));
        Truth.assertThat(register).isNotNull();
        Truth.assertThat(register.getUid()).isNotNull();

        CompareObjectUtils.compare(register, dto);
    }

    @Test
    void givenNullRequest_whenMapToAccountDto_thenReturnNull() {
        Truth.assertThat(RegisterMapper.INSTANCE.toAccountDto(null, "any")).isNull();
    }

    @Test
    void givenRequestWithoutRemoteAddress_whenMapToAccountDto_thenReturnNull() {
        for (String invalidStringValue : TestUtils.getInvalidStringValues()) {
            RegisterRequestDto request = DtoFactoryUtils.registerRequest();
            AccountDto account = RegisterMapper.INSTANCE.toAccountDto(request, invalidStringValue);
            Truth.assertThat(account).isNotNull();
            CompareObjectUtils.compare(request, account);
        }
    }

}